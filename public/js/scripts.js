var isModelOpen = false;
var lastScroll = 0;
var secondNavbar = false;

function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
}

$(document).ready(function($) {

    $(".dropdown > li").on('click', function() {
        var $container = $(this),
            $list = $container.find("ul"),
            $anchor = $container.find("a");
        if(!$list.is(":visible") && $list.is(":hidden")){
            $anchor.addClass("hover");
            $list
                .show()
                .css({
                    paddingTop: $container.data("origHeight")
                });
        }else{
            $list.hide();
            $anchor.removeClass("hover");
        }
    });

    $(".dropdown > li").hover(function() {

        var $container = $(this),
            $list = $container.find("ul"),
            $anchor = $container.find("a");
        $container.data("origHeight", $container.height());
        $anchor.addClass("hover");
        $list
            .show()
            .css({
                paddingTop: $container.data("origHeight")
            });
    }, function() {

        var $el = $(this);

        $el
            .height($(this).data("origHeight"))
            .find("ul")
            .css({ top: 0 })
            .hide()
            .end()
            .find("a")
            .removeClass("hover");

    });

    $('.trigger-recall-modal').on('click', function() {
        $('#modal-recall').toggleClass('open');
        $('.wrapper').toggleClass('blur-it');
        isModelOpen = true;
        return false;
    });

    $('.trigger-commissioner-modal').on('click', function() {
        $('#modal-commissioner').toggleClass('open');
        $('.wrapper').toggleClass('blur-it');
        isModelOpen = true;
        return false;
    });

    $('.trigger-specialist-modal').on('click', function() {
        $('#modal-specialist').toggleClass('open');
        $('.wrapper').toggleClass('blur-it');
        isModelOpen = true;
        return false;
    });

    $("#lightgallery").lightGallery({
        mode: 'lg-fade',
        cssEasing : 'cubic-bezier(0.25, 0, 0.25, 1)',
        loop:true,
        escKey:true,
        thumbnail:true,
        autoplay:true,
        zoom:true,
    });

    $('#calc-type-select').select2({
        minimumResultsForSearch: -1,
        theme: "default"
    }).on('select2:select', function (e) {
        var data = e.params.data;
        if(data['id']){
            $(location).attr('href', '/calculator/'+data['id']);
        }
    });

    $('img.captcha-img').on('click', function () {
        var captcha = $(this);
        var config = captcha.data('refresh-config');
        $.ajax({
            method: 'GET',
            url: '/get_captcha/' + config,
        }).done(function (response) {
            captcha.prop('src', response);
        });
    });

    if(window.location.href.indexOf("calculator") !== -1) {
        getResultCalc();

        $('#form-calc-result input[type=number]').each(function(){
            $(this).change(function() {
                getResultCalc();
            });
        });

        $('#form-calc-result input[type=checkbox]').each(function(){
            $(this).change(function() {
                getResultCalc();
            });
        });

        $('#form-calc-result select').each(function(){
            $(this).select2({
                minimumResultsForSearch: -1,
                theme: "default"
            }).on('select2:select', function (e) {
                getResultCalc();
            });
        });

        function getResultCalc() {
            $.ajax({
                method: 'post',
                url: '/get-result',
                data : $("#form-calc-result").serializeArray(),
                success: function (html) {
                    $("#premium-price").html(html);
                }
            });
        }
    }



    if(window.location.href.indexOf("faq") !== -1){
        var MqM= 768,
            MqL = 1024;

        var faqsSections = $('.cd-faq-group'),
            faqTrigger = $('.cd-faq-trigger'),
            faqsContainer = $('.cd-faq-items'),
            faqsCategoriesContainer = $('.cd-faq-categories'),
            faqsCategories = faqsCategoriesContainer.find('a'),
            closeFaqsContainer = $('.cd-close-panel');

        faqsCategories.on('click', function(event){
            event.preventDefault();
            var selectedHref = $(this).attr('href'),
                target= $(selectedHref);

                $('body,html').animate({ 'scrollTop': target.offset().top - 150}, 200);
        });

        $('body').bind('click touchstart', function(event){
            if( $(event.target).is('body.cd-overlay') || $(event.target).is('.cd-close-panel')) {
                closePanel(event);
            }
        });
        faqsContainer.on('swiperight', function(event){
            closePanel(event);
        });

        faqTrigger.on('click', function(event){
            event.preventDefault();
            $(this).next('.cd-faq-content').slideToggle(200).end().parent('li').toggleClass('content-visible');
        });

        $(window).on('scroll', function(){
            if ( $(window).width() > MqL ) {
                (!window.requestAnimationFrame) ? updateCategory() : window.requestAnimationFrame(updateCategory);
            }
        });

        $(window).on('resize', function(){
            if($(window).width() <= MqL) {
                faqsCategoriesContainer.removeClass('is-fixed').css({
                    '-moz-transform': 'translateY(0)',
                    '-webkit-transform': 'translateY(0)',
                    '-ms-transform': 'translateY(0)',
                    '-o-transform': 'translateY(0)',
                    'transform': 'translateY(0)',
                });
            }
            if( faqsCategoriesContainer.hasClass('is-fixed') ) {
                faqsCategoriesContainer.css({
                    'left': faqsContainer.offset().left,
                });
            }
        });

        function closePanel(e) {
            e.preventDefault();
            faqsContainer.removeClass('slide-in').find('li').show();
            closeFaqsContainer.removeClass('move-left');
            $('body').removeClass('cd-overlay');
        }

        function updateCategory(){
            updateCategoryPosition();
            updateSelectedCategory();
        }

        function updateCategoryPosition() {
            var top = $('.cd-faq').offset().top,
                height = $('.cd-faq').height() - $('.cd-faq-categories').height(),
                margin = 100;
            if( top - margin <= $(window).scrollTop() && top - margin + height > $(window).scrollTop() ) {
                var leftValue = faqsCategoriesContainer.offset().left,
                    widthValue = faqsCategoriesContainer.width();
                faqsCategoriesContainer.addClass('is-fixed').css({
                    'left': leftValue,
                    'top': margin,
                    '-moz-transform': 'translateZ(0)',
                    '-webkit-transform': 'translateZ(0)',
                    '-ms-transform': 'translateZ(0)',
                    '-o-transform': 'translateZ(0)',
                    'transform': 'translateZ(0)',
                });
            } else if( top - margin + height <= $(window).scrollTop()) {
                var delta = top - margin + height - $(window).scrollTop();
                faqsCategoriesContainer.css({
                    '-moz-transform': 'translateZ(0) translateY('+delta+'px)',
                    '-webkit-transform': 'translateZ(0) translateY('+delta+'px)',
                    '-ms-transform': 'translateZ(0) translateY('+delta+'px)',
                    '-o-transform': 'translateZ(0) translateY('+delta+'px)',
                    'transform': 'translateZ(0) translateY('+delta+'px)',
                });
            } else {
                faqsCategoriesContainer.removeClass('is-fixed').css({
                    'left': 0,
                    'top': 0,
                });
            }
        }

        function updateSelectedCategory() {
            faqsSections.each(function(){
                var actual = $(this),
                    margin = parseInt($('.cd-faq-title').eq(1).css('marginTop').replace('px', '')),
                    activeCategory = $('.cd-faq-categories a[href="#'+actual.attr('id')+'"]'),
                    topSection = (activeCategory.parent('li').is(':first-child')) ? 0 : Math.round(actual.offset().top);

                if ( ( topSection - 200 <= $(window).scrollTop() ) && ( Math.round(actual.offset().top) + actual.height() + margin - 200 > $(window).scrollTop() ) ) {
                    activeCategory.addClass('selected');
                }else {
                    activeCategory.removeClass('selected');
                }
            });
        }
    }
});

$( document ).ready(function() {
    $('#scrollup img').mouseover( function(){
        $( this ).animate({opacity: 0.65},1100);
    }).mouseout( function(){
        $( this ).animate({opacity: 1},1100);
    }).click( function(){
        $('html, body').animate({scrollTop: 0},500);
        return false;
    });

    $(window).scroll(function(){
        if ( $(document).scrollTop() > 0 ) {
            $('#scrollup').fadeIn('slow');
        } else {
            $('#scrollup').fadeOut('slow');
        }
    });
});

$(document).on('click', 'a[href*="#"]', function (event) {
    if($($.attr(this, 'href')) && window.location.href.indexOf("faq") == -1){
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 100
        }, 500);
    }

});

$("#menu-toggle").click(function(e) {
    this.classList.toggle("is-active");
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
    $(".brand-block").toggleClass("hide");
    $("#navigation").fadeToggle()
});