<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalcFormulaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calc_formula', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calc_type_id')->unsigned();
            $table->string('formula');
            $table->timestamps();

            $table->foreign('calc_type_id')->references('id')->on('calc_type');
            $table->dropForeign(['calc_type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calc_formula');
    }
}
