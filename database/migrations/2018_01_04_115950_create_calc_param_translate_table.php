<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalcParamTranslateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calc_param_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calc_param_id')->unsigned();
            $table->string('locale');
            $table->string('placeholder');
            $table->string('help_text');
            $table->timestamps();

            $table->foreign('calc_param_id')->references('id')->on('calc_param');
            $table->dropForeign(['calc_param_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calc_param_translate');
    }
}
