<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalcParamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calc_param', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('action');
            $table->integer('bonds_field')->nullable();
            $table->double('percent')->nullable();
            $table->double('value')->nullable();
            $table->integer('type');
            $table->string('placeholder')->nullable();
            $table->string('help_text')->nullable();
            $table->string('read_only');
            $table->string('default_value')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calc_param');
    }
}
