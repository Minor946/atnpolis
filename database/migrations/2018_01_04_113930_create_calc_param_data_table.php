<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalcParamDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calc_param_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calc_param_id')->unsigned();
            $table->string('name');
            $table->double('percent');
            $table->double('value');
            $table->timestamps();


            $table->foreign('calc_param_id')->references('id')->on('calc_param');
            $table->dropForeign(['calc_param_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calc_param_data');
    }
}
