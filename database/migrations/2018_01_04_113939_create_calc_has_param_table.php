<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalcHasParamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calc_has_param', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calc_type_id')->unsigned();
            $table->integer('calc_param_id')->unsigned();
            $table->integer('position');
            $table->timestamps();

            $table->foreign('calc_type_id')->references('id')->on('calc_type');
            $table->dropForeign(['calc_type_id']);

            $table->foreign('calc_param_id')->references('id')->on('calc_param');
            $table->dropForeign(['calc_param_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calc_has_param');
    }
}
