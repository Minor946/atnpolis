<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('item');
            $table->string('locale');
            $table->string('alias');
            $table->string('name')->nullable();
            $table->string('short_desc')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_models');
    }
}
