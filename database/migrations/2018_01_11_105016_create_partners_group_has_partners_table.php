<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersGroupHasPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners_group_has_partners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('partners_id')->unsigned();
            $table->integer('partners_group_id')->unsigned();
            $table->timestamps();

            $table->foreign('partners_id')->references('id')->on('partners');
            $table->dropForeign(['partners_id']);

            $table->foreign('partners_group_id')->references('id')->on('partners_group');
            $table->dropForeign(['partners_group_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners_group_has_partners');
    }
}
