<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/8/18
 * Time: 12:25 PM
 */

return [
    'default'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
    ],
];