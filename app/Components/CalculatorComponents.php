<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/5/18
 * Time: 2:33 PM
 */

namespace App\Components;


use App\Models\Calculator;
use App\Models\CalculatorParameterData;
use App\Models\CalculatorParameters;

class CalculatorComponents
{

    public static function getSelectedName($type,$locale)
    {
        $name = null;
        if(empty($type)){
            $calc = Calculator::where('status', Calculator::CALC_SHOW)->orderBy('position')->first();
            if(!empty($calc)){
              $name =  TranslateComponents::getName($calc->id,TranslateComponents::TYPE_CALC, $locale);
            }
        }else{
            $calc = Calculator::where('alias', $type)->first();
            if(!empty($calc)){
                $name = TranslateComponents::getName($calc->id,TranslateComponents::TYPE_CALC,$locale);
            }
        }
        return $name;
    }

    public static function checkIfNull($value)
    {
        if(empty($value)){
            return 0;
        }
        return $value;
    }

    public static function getValueByParam($value, $key, $extra = null)
    {
        $calc = CalculatorParameters::find($key);
        if(!empty($calc)){
                if($calc->type == CalculatorParameters::TYPE_SELECT){
                    $param = CalculatorParameterData::find($value);
                    if(!empty($param)){
                        if(!empty($param->percent)){
                            return $param->percent;
                        }elseif(!empty($param->value)){
                            return $param->value;
                        }elseif (!empty($param->value_array)){
                            return json_decode($param->value_array, true);
                        }
                    }
                }
                if($calc->type == CalculatorParameters::TYPE_NUMBER){
                    if(!empty($calc->percent)){
                        return $value * $calc->percent;
                    }
                    return $value;
                }
                if($calc->type == CalculatorParameters::TYPE_CHECKBOX){
                    if(!empty($calc->percent)){
                        return $calc->percent;
                    }else{
                        return $calc->value;
                    }
                }
                if($calc->type == CalculatorParameters::TYPE_HIDE){
                    if(!empty($calc->percent)){
                        return $calc->percent;
                    }else{
                        return $calc->value;
                    }
                }
        }
        return 0;
    }

    public static function getIcon($alias)
    {
        if($alias == Calculator::type_ocgop){
            return "fa-user";
        }
        if($alias == Calculator::type_ocgopo){
            return "fa-fire";
        }
        if($alias == Calculator::type_ocgopog){
            return "fa-truck";
        }
        if($alias == Calculator::type_ocgpopp){
            return "fa-bus";
        }
    }
}