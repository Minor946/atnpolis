<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/10/18
 * Time: 4:58 PM
 */

namespace App\Components;


use App\Models\Services;

class ServiceComponents
{

    public static function getLogoName($service_id)
    {
        $service = Services::find($service_id);
        if(!empty($service) && !empty($service->logo)){
            return  "../uploads/".$service->logo;
        }
        return "/images/stub.png";
    }

    public static function getAlias($service_id)
    {
        $service = Services::find($service_id);
        if(!empty($service)){
            return  $service->alias;
        }
        return "";
    }
}