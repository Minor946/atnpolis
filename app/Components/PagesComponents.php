<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/12/18
 * Time: 4:39 PM
 */

namespace App\Components;


use App\Models\Calculator;
use App\Models\Pages;
use App\Models\Post;
use App\Models\SearchModel;
use App\Models\Services;
use App\Models\Translation;

class PagesComponents
{
    public static function getPageContent($alias)
    {
        $page = Pages::findByAlias($alias);
        if(!empty($page)){
            return  $page->description;
        }
        return "";
    }

   public static function updatedSearchAll(){
        self::updateSearchPost();
        self::updateSearchCalc();
        self::updateSearchService();
        self::updateSearchPages();
   }

    private static function updateSearchPages(){
        $modelService = Pages::all();
        if(!empty($modelService)){
            foreach ($modelService as $item){
                self::createSearchRow($item->name, $item->description, $item->id, "ru", "pages" , $item->alias, $item->short_desc);
                $modelTranslate = Translation::where('item', $item->id)->where('type',TranslateComponents::TYPE_PAGES)->get();
                if(!empty($modelTranslate)){
                    foreach ($modelTranslate as $value) {
                        self::createSearchRow($value->name, $value->description, $item->id, $value->locale, "pages", $item->alias, $value->sub_item);
                    }
                }
            }
        }
    }

    private static function updateSearchPost(){
        $modelService = Post::all();
        if(!empty($modelService)){
            foreach ($modelService as $item){
                self::createSearchRow($item->title, $item->description, $item->id, "ru", "post" , $item->alias, $item->short_desc);
                $modelTranslate = Translation::where('item', $item->id)->where('type',TranslateComponents::TYPE_POST)->get();
                if(!empty($modelTranslate)){
                    foreach ($modelTranslate as $value) {
                        self::createSearchRow($value->name, $value->description, $item->id, $value->locale, "post", $item->alias, $value->sub_item);
                    }
                }
            }
        }
    }

    private static function updateSearchCalc(){
        $modelService = Calculator::all();
        if(!empty($modelService)){
            foreach ($modelService as $item){
                self::createSearchRow($item->name, $item->description, $item->id, "ru", "calc" , $item->alias);
                $modelTranslate = Translation::where('item', $item->id)->where('type',TranslateComponents::TYPE_CALC)->get();
                if(!empty($modelTranslate)){
                    foreach ($modelTranslate as $value) {
                        self::createSearchRow($value->name, $value->description, $item->id, $value->locale, "calc", $item->alias);
                    }
                }
            }
        }
    }

    private static function updateSearchService(){
        $modelService = Services::all();
        if(!empty($modelService)){
            foreach ($modelService as $item){
                self::createSearchRow($item->name, $item->description, $item->id, "ru", "service", $item->alias );
                $modelTranslate = Translation::where('item', $item->id)->where('type',TranslateComponents::TYPE_SERVICE)->get();
                if(!empty($modelTranslate)){
                    foreach ($modelTranslate as $value) {
                        self::createSearchRow($value->name, $value->description, $item->id, $value->locale, "service", $item->alias, "service");
                    }
                }
            }
        }
    }

    private static function createSearchRow($name, $description, $id, $locale, $type, $alias, $sub_item = null)
    {
        $model = SearchModel::where('item', $id)->where('locale',$locale)->where('type', $type)->first();
        if(!empty($model)){
            $model->name = $name;
            $model->description = $description;
            $model->short_desc = $sub_item;
            $model->alias = $alias;
            $model->save();
        }else{
            $model = new SearchModel();
            $model->name = $name;
            $model->description = $description;
            $model->item = $id;
            $model->locale = $locale;
            $model->type = $type;
            $model->short_desc = $sub_item;
            $model->alias = $alias;
            $model->save();
        }
    }


}