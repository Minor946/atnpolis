<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/5/18
 * Time: 2:34 PM
 */

namespace App\Components;


use App\Models\Calculator;
use App\Models\CalculatorParameterData;
use App\Models\CalculatorParameters;
use App\Models\CalculatorTranslate;
use App\Models\Pages;
use App\Models\Partners;
use App\Models\PartnersGroups;
use App\Models\Post;
use App\Models\Services;
use App\Models\Sliders;
use App\Models\Subsidiary;
use App\Models\Translation;
use App\Models\Workers;

class TranslateComponents
{
    const TYPE_CALC = 'calc';//name
    const TYPE_CALC_PARAM_DATA = 'calc_param_data';//name
    const TYPE_PARTNERS_GROUP_NAME = 'partners_group';//name
    const TYPE_PARTNERS_NAME = 'partners';//name

    const TYPE_POST = 'post';//title, description, short_desc to sub_item
    const TYPE_SUBSIDIARIES = 'subsidiaries';//address, message
    const TYPE_WORKERS = 'workers';//initials, post

    const TYPE_PAGES = 'pages';//name, description
    const TYPE_SERVICE = 'service';//name, description
    const TYPE_SLIDER = 'slider';//name, description

    const TYPE_CALC_PARAM = 'calc_param';//name, and into sub_item put param placeholder and help-text

    const ALL_TYPE = [ 'calc', 'calc_param_data', 'partners_group', 'partners', 'post', 'subsidiaries', 'calc_param', 'pages', 'service', 'slider','workers'];

    const ALL_LOCALE = [ 'en', 'ky', 'ty'];


    public static function getName($id, $type, $locale)
    {
        if($type == self::TYPE_CALC){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->name)){
                return $item->name;
            }else{
                return Calculator::find($id)->name;
            }
        }elseif ($type == self::TYPE_SERVICE){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->name)){
                return $item->name;
            }else{
                return Services::find($id)->name;
            }
        }
        elseif ($type == self::TYPE_CALC_PARAM){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->name)){
                return $item->name;
            }else{
                return CalculatorParameters::find($id)->name;
            }
        }elseif ($type == self::TYPE_CALC_PARAM_DATA){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->name)){
                return $item->name;
            }else{
                return CalculatorParameterData::find($id)->name;
            }
        }elseif ($type == self::TYPE_PARTNERS_GROUP_NAME){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->name)){
                return $item->name;
            }else{
                return PartnersGroups::find($id)->name;
            }
        }elseif ($type == self::TYPE_PARTNERS_NAME){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->name)){
                return $item->name;
            }else{
                return Partners::find($id)->name;
            }
        }elseif ($type == self::TYPE_PAGES){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->name)){
                return $item->name;
            }else{
                return Pages::find($id)->name;
            }
        }elseif ($type == self::TYPE_POST){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->name)){
                return $item->name;
            }else{
                return Post::find($id)->title;
            }
        }elseif ($type == self::TYPE_SUBSIDIARIES){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->name)){
                return $item->name;
            }else{
                return Subsidiary::find($id)->address;
            }
        }elseif ($type == self::TYPE_WORKERS){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->name)){
                return $item->name;
            }else{
                return Workers::find($id)->initials;
            }
        }
    }

    public static function getPostShortDesc($id, $locale){
        $item = Translation::where('locale', $locale)
            ->where('type', self::TYPE_POST)
            ->where('item', $id)
            ->first();
        if(!empty($item) && !empty($item->sub_item)){
            return $item->sub_item;
        }else{
            return Post::find($id)->short_desc;
        }
    }

    public static function getDesc($id, $type, $locale){
        if ($type == self::TYPE_SUBSIDIARIES){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->description)){
                return $item->description;
            }else{
                return Subsidiary::find($id)->message;
            }
        }elseif ($type == self::TYPE_WORKERS){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->description)){
                return $item->description;
            }else{
                return Workers::find($id)->post;
            }
        }elseif ($type == self::TYPE_PAGES){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->description)){
                return $item->description;
            }else{
                return Pages::find($id)->description;
            }
        }elseif ($type == self::TYPE_POST){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->description)){
                return $item->description;
            }else{
                return Post::find($id)->description;
            }
        }elseif ($type == self::TYPE_SERVICE){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->description)){
                return $item->description;
            }else{
                return Services::find($id)->description;
            }
        }elseif ($type == self::TYPE_CALC){
            $item = Translation::where('locale', $locale)
                ->where('type', $type)
                ->where('item', $id)
                ->first();
            if(!empty($item) && !empty($item->description)){
                return $item->description;
            }else{
                return Calculator::find($id)->description;
            }
        }
    }


    public static function getPlaceholder($id, $locale)
    {
        $item = CalculatorTranslate::where('calc_param_id',$id)
            ->where('locale', $locale)
            ->first();
        if(!empty($item)){
            return $item->placeholder;
        }else{
            return CalculatorParameters::find($id)->placeholder;
        }
    }

    public static function getHelpText($id, $locale)
    {
        $item = CalculatorTranslate::where('calc_param_id',$id)
            ->where('locale', $locale)
            ->first();
        if(!empty($item)){
            return $item->help_text;
        }else{
            return CalculatorParameters::find($id)->help_text;
        }
    }

    /**
     * @param $string String rus
     * @return string
     */
    public static function getAlias($string){
        $s = (string) $string;
        $s = strip_tags($s);
        $s = str_replace(array("\n", "\r"), " ", $s);
        $s = preg_replace("/\s+/", ' ', $s);
        $s = trim($s);
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s);
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s);
        $s = str_replace(" ", "-", $s);
        return $s;
    }

    public static function getTownName($region, $locale)
    {
        if($locale == 'ty'){
            $towns = [
                '0' => "Бишкек",
                '1' => "Ош",
                '2' => "Талас",
                '3' => "Баткен",
                '4' => "Джалал-Абад",
                '5' => "Каракол",
            ];
        }elseif ($locale == 'en'){
            $towns = [
                '0' => "Bishkek",
                '1' => "Osh",
                '2' => "Талас",
                '3' => "Баткен",
                '4' => "Джалал-Абад",
                '5' => "Каракол",
            ];
        }elseif ($locale == 'ky'){
            $towns = [
                '0' => "Бишкек",
                '1' => "Ош",
                '2' => "Талас",
                '3' => "Баткен",
                '4' => "Джалал-Абад",
                '5' => "Каракол",
            ];
        }else{
            $towns = [
                '0' => "Бишкек",
                '1' => "Ош",
                '2' => "Талас",
                '3' => "Баткен",
                '4' => "Джалал-Абад",
                '5' => "Каракол",
            ];
        }

        return $towns[$region];
    }

    public static function createAllTranslate(){
        $count = 0;
        foreach (self::ALL_TYPE as $type){
            if($type != self::TYPE_CALC_PARAM){
                $models = self::getAllModelsByType($type);
                if(!empty($models)){
                    foreach ($models as $item) {
                        foreach (self::ALL_LOCALE as $locale){
                            if(!self::isCreate($locale,$type,$item->id)){
                                self::createTranslate($locale,$type,$item->id);
                                $count++;
                            }
                        }
                    }
                }
            }else{
                $models = CalculatorParameters::all();
                if(!empty($models)){
                    foreach ($models as $item){
                        foreach (self::ALL_LOCALE as $locale){
                            if(!self::isCreate($locale,$type,$item->id)){
                                $count++;
                                self::createTranslate($locale,$type,$item->id);
                                self::createTranslate($locale,$type,$item->id,'help_text');
                                self::createTranslate($locale,$type,$item->id,'placeholder');
                            }
                        }
                    }
                }
            }
        }
    }

    private static function isCreate($locale, $type, $item_id, $sub_item = null)
    {
        if(!empty($sub_item)){
            $translate = Translation::where('locale', $locale)->where('type',$type)->where('item',$item_id)->where('sub_item', $sub_item)->first();
        }else{
            $translate = Translation::where('locale', $locale)->where('type',$type)->where('item',$item_id)->first();
        }
        if(!empty($translate)){
            return true;
        }
        return false;
    }

    private static function createTranslate($locale, $type, $item_id, $sub_item = null)
    {
        $model = new Translation();
        $model->locale = $locale;
        $model->type = $type;
        $model->item = $item_id;
        $model->sub_item = $sub_item;
        $model->save();
    }

    private static function getModelsByType($type, $id)
    {
        switch ($type){
            case self::TYPE_CALC:
                return Calculator::find($id);
            case self::TYPE_CALC_PARAM_DATA:
                return CalculatorParameterData::find($id);
            case self::TYPE_PARTNERS_GROUP_NAME:
                return PartnersGroups::find($id);
            case self::TYPE_PARTNERS_NAME:
                return Partners::find($id);
            case self::TYPE_POST:
                return Post::find($id);
            case self::TYPE_SUBSIDIARIES:
                return Subsidiary::find($id);
            case self::TYPE_PAGES:
                return Pages::find($id);
            case self::TYPE_SERVICE:
                return Services::find($id);
            case self::TYPE_SLIDER:
                return Sliders::find($id);
            case self::TYPE_WORKERS:
                return Workers::find($id);
            default:
                return null;
        }
    }

    public static function getLastByType($id, $type)
    {
       return self::getModelsByType($type, $id);
    }


    private static function getAllModelsByType($type)
    {
        switch ($type){
            case self::TYPE_CALC:
                return Calculator::all();
            case self::TYPE_CALC_PARAM_DATA:
                return CalculatorParameterData::all();
            case self::TYPE_PARTNERS_GROUP_NAME:
                return PartnersGroups::all();
            case self::TYPE_PARTNERS_NAME:
                return Partners::all();
            case self::TYPE_POST:
                return Post::all();
            case self::TYPE_SUBSIDIARIES:
                return Subsidiary::all();
            case self::TYPE_PAGES:
                return Pages::all();
            case self::TYPE_SERVICE:
                return Services::all();
            case self::TYPE_SLIDER:
                return Sliders::all();
            case self::TYPE_WORKERS:
                return Workers::all();
            default:
                return null;
        }
    }

}