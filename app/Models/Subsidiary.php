<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subsidiary extends Model
{

    const STATUS_SHOW = 0;
    const STATUS_HIDE = 1;

    const TYPE_MAIN = 0;
    const TYPE_OTHER = 1;

    //
    protected $table = "subsidiaries";

}
