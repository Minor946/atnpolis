<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partners extends Model
{
    //
    const STATUS_SHOW = 0;
    const STATUS_HIDE = 1;

    protected $table = 'partners';

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($partners) {
            $partners->hasGroup()->delete();
        });
    }

    public function hasGroup(){
        return $this->hasOne(PartnersHasGroups::class,'partners_id');
    }
}
