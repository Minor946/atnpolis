<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/12/18
 * Time: 4:42 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PagesContent extends Model
{

    protected $table = 'pages_content';

    public static function findByPageId($id)
    {
        $content = PagesContent::where('page_id', $id)->first();
        if(!empty($content)){
            return $content;
        }
        return null;
    }


}