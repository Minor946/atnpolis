<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/23/18
 * Time: 2:53 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CalculatorFormula extends Model
{

    protected $table = 'calc_formula';

    public static function formulaByType($calc_type_id)
    {
        $formula = CalculatorFormula::where('calc_type_id', $calc_type_id)->first();
        if(!empty($formula)){
            return $formula->formula;
        }
        return null;
    }
}