<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    const TYPE_NEWS = 0;
    const TYPE_BLOG = 1;

    const STATUS_SHOW = 0;
    const STATUS_HIDE = 1;

    protected $table = 'posts';

    public static function whereAlias($alias)
    {
        $post = Post::where('alias', $alias)->first();
        if(!empty($post)){
            return $post;
        }
        return null;
    }

    public static function getColor($type)
    {
        if($type == Post::TYPE_BLOG){
            return "bg-blog";
        }else{
            return "bg-news";
        }
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($post) {
            $post->translate()->delete();
            $post->assets()->delete();
        });
    }


    public function translate(){
        return $this->hasOne(PostTranslate::class);
    }

    public function assets(){
        return $this->hasMany(PostAssets::class);
    }

    public function setPostGalleryAttribute($pictures)
    {
        if (is_array($pictures)) {
            $this->attributes['post_gallery'] = json_encode($pictures);
        }
    }

    public function getPostGalleryAttribute($pictures)
    {
        return json_decode($pictures, true);
    }

}
