<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/23/18
 * Time: 2:43 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CalculatorParameterData extends Model
{
    protected $table = 'calc_param_data';

}