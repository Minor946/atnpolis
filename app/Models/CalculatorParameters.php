<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/23/18
 * Time: 2:27 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CalculatorParameters  extends Model
{
    const ACTION_MULTIPLY = 0;

    const TYPE_SELECT = 1;
    const TYPE_NUMBER = 2;
    const TYPE_CHECKBOX = 3;
    const TYPE_HIDE = 4;

    const READONLY = 0;
    const NOREADONLY = 1;

    protected $table = 'calc_param';

    public static function paramByType($calc_type)
    {
        $calc = Calculator::where('alias', $calc_type)->first();
        if(!empty($calc)){
            $params = CalculatorHasParam::where('calc_type_id',$calc->id)->orderBy('position')->get();
            if(!empty($params)){
                return $params;
            }
        }
        return null;
    }

}