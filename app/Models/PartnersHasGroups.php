<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnersHasGroups extends Model
{
    //
    protected $table = 'partners_group_has_partners';

    public function partners()
    {
        return $this->belongsTo(Partners::class,'partners_id');
    }

    public function groups()
    {
        return $this->belongsTo(PartnersGroups::class,'partners_group_id');
    }

    public function hasGroup(){
        return $this->hasOne(PartnersHasGroups::class,'partners_group_id');
    }

    public function hasPartners(){
        return $this->hasOne(Partners::class,'partners_id');
    }
    
}
