<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallBackOffer extends Model
{
    //

    protected $table = 'call_backs_offer';

    public static function getType($type, $callback_id){
        $type = CallBackOffer::where('name', $type)->where('callback_id',$callback_id)->first();
        if(!empty($type)){
            return $type->value;
        }
        return null;
    }

    public function callback()
    {
        return $this->belongsTo(CallBack::class, 'callback_id');
    }
}
