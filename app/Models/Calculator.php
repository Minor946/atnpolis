<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calculator extends Model
{
    //

    const CALC_HIDE = 0;
    const CALC_SHOW = 1;

    const type_ocgop = "ocgop";
    const type_ocgopo = "ocgopo";
    const type_ocgopog = "ocgopog";
    const type_ocgpopp = "ocgpopp";

    protected $table = 'calc_type';

    public static function whereType($type)
    {
        $calc = Calculator::where('alias', $type)->first();
        if(!empty($calc)){
            return $calc;
        }
        return null;
    }

    public static function wherePosition($position)
    {
        $calc = Calculator::where('position', $position)->first();
        if(!empty($calc)){
            return $calc;
        }
        return null;
    }

}
