<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/23/18
 * Time: 3:32 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CalculatorHasParam extends Model
{
    protected $table = 'calc_has_param';

}