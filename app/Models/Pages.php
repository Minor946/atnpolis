<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{

    const STATUS_SHOW = 0;
    const STATUS_HIDE = 1;

    const TYPE_COMMON = 0;
    const TYPE_FAQ = 1;

    //
    public static function findByAlias($alias)
    {
        $page = Pages::where('alias', $alias)->first();
        if(!empty($page)){
            return $page;
        }
        return null;
    }
}
