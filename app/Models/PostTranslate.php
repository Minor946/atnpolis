<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTranslate extends Model
{

    protected $table = 'post_translate';

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

}
