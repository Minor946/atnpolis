<?php

namespace App\Models;

use App\Admin\Controllers\PartnersController;
use Illuminate\Database\Eloquent\Model;

class PostAssets extends Model
{
    //

    const TYPE_VIDEO = 1;
    const TYPE_GALLERY = 2;

    protected $table = 'post_assets';

    public static function createGallery($name, $post_id)
    {
        $asset = new PostAssets();
        $asset->post_id = $post_id;
        $asset->value = $name;
        $asset->type = self::TYPE_GALLERY;
        $asset->save();
    }

    public static function getGallery($post_id)
    {
        $gallery = PostAssets::where('type', self::TYPE_GALLERY)->where('post_id', $post_id)->get();
        if(!empty($gallery)){
            return $gallery;
        }
        return null;
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
