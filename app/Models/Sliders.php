<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    //
    protected $table = 'main_slider';

    const TYPE_IMG = 0;
    const TYPE_VIDEO = 1;

    const STATUS_SHOW = 0;
    const STATUS_HIDE = 1;


    const TEXT_CENTER = 'center';
    const TEXT_LEFT = "left";
    const TEXT_RIGHT = 'right';
}
