<?php

namespace App\Models;

use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class CallBack extends Model
{
    use FormAccessible;

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($post) {
            $post->offer()->delete();
        });
    }

    const TYPE_RECALL = 0;
    const TYPE_SPECIALIST = 1;
    const TYPE_WRITE_US = 2;
    const TYPE_COMMISSIONER = 3;
    const TYPE_CALCULATOR = 4;

    const STATUS_NEW = 0;
    const STATUS_CHECK = 1;
    const STATUS_DECLINE = 2;

    //
    protected $table = 'call_backs';

    public static function getNameByType($type)
    {
        if($type == self::TYPE_RECALL){
            return ['type'=>'warning','name'=>"Обратная связь"];
        }
        if($type == self::TYPE_SPECIALIST){
            return ['type'=>'primary','name'=>"Специалист"];
        }
        if($type == self::TYPE_WRITE_US){
            return ['type'=>'info','name'=>"Напишите нам"];
        }
        if($type == self::TYPE_COMMISSIONER){
            return ['type'=>'danger','name'=>"Коммисар"];
        }
        if($type == self::TYPE_CALCULATOR){
            return ['type'=>'success','name'=>"Калькулятор"];
        }
    }

    public function getDateOfBirthAttribute($value)
    {
        return Carbon::parse($value)->format('m/d/Y');
    }

    public function formDateOfBirthAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function offer(){
        return $this->hasMany(CallBackOffer::class,'callback_id');
    }
}
