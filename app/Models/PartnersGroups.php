<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnersGroups extends Model
{
    //
    protected $table = 'partners_group';

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($group) {
            $group->hasPartner()->delete();
        });
    }

    public function hasPartner(){
        return $this->hasOne(PartnersHasGroups::class,'partners_group_id');
    }

}
