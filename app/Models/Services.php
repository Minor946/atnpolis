<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    //
    const TYPE_PRIVATE = 0;
    const TYPE_CORPORATE = 1;

    const SERVICE_HIDE = 0;
    const SERVICE_SHOW = 1;

    protected $table = 'services';

    public static function whereAlias($alias)
    {
        $service = Services::where('alias', $alias)->first();
        if(!empty($service)){
            return $service;
        }
        return null;
    }

    public function setGalleryAttribute($pictures)
    {
        if (is_array($pictures)) {
            $this->attributes['gallery'] = json_encode($pictures);
        }
    }

    public function getGalleryAttribute($pictures)
    {
        return json_decode($pictures, true);
    }
}
