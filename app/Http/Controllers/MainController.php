<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/12/17
 * Time: 1:22 PM
 */

namespace App\Http\Controllers;


use App\Components\CalculatorComponents;
use App\Components\TranslateComponents;
use App\Models\Calculator;
use App\Models\CallBack;
use App\Models\CallBackOffer;
use App\Models\Pages;
use App\Models\Post;
use App\Models\SearchModel;
use App\Models\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    public function index(){
        return view('index');
    }

    public function calculator($type = null){
        if(!empty(Calculator::whereType($type))){
            return view('calculator', ['type' => $type]);
        }else{
            $calc = Calculator::wherePosition(1);
            return view('calculator', ['type' => $calc->alias]);
        }
    }

    public function post($alias){
        if(!empty(Post::whereAlias($alias))){
            return view('post',['alias' => $alias]);
        }else{
            return  abort(404);
        }
    }

    public function service($alias){
        if(!empty(Services::whereAlias($alias)) || $alias == "private_clients" ||  $alias == "corporate_clients"){
            return view('service',['alias' => $alias]);
        }else{
            return  abort(404);
        }
    }

    public function about(){
        return view('about');
    }

    public function pages($alias){
        if(!empty(Pages::findByAlias($alias))){
            return view('terms', ['type' => $alias]);
        }else{
            return  abort(404);
        }
    }

    public function faq(){
        return view('faq');
    }

    public function blog(){
        return view('blog',['type'=>\App\Models\Post::TYPE_BLOG]);
    }
    public function news(){
        return view('blog',['type'=>\App\Models\Post::TYPE_NEWS]);
    }

    public function moreblog(Request $request){
        $data = $request->all();
        $page = (int) $data['page'] + 1;
        $canMore = false;
        return view('components.blog.blog_more', ['page'=>$page, 'canMore'=>$canMore]);
    }


    public function contacts(){
        return view('contacts');
    }

    public function employees(){
        return view('employees');
    }

    public function partners(){
        return view('partners');
    }

    public function captha(){
            $rules = ['captcha' => 'required|captcha'];
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails())
            {
                echo '<p style="color: #ff0000;">Incorrect!</p>';
            }
            else
            {
                echo '<p style="color: #00ff30;">Matched :)</p>';
            }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function result(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $calc = Calculator::whereType($request->input('input-type'));
            foreach ($data as $key => $item) {
                $data[$key] = CalculatorComponents::checkIfNull($item);
            }
            if(!empty($calc) && $calc->alias == Calculator::type_ocgop){
                $result = (
                        CalculatorComponents::getValueByParam($data['input-2'], 2) / 100 +
                        (
                                CalculatorComponents::getValueByParam($data['input-3'], 3) *
                                (CalculatorComponents::getValueByParam($data['input-1'], 1) / 100)
                        ) +
                        CalculatorComponents::getValueByParam($data['input-4'], 4) / 100
                    ) *
                    CalculatorComponents::getValueByParam($data['input-5'], 5);
                $check[] = CalculatorComponents::getValueByParam($data['input-2'], 2);
                $check[] = CalculatorComponents::getValueByParam($data['input-3'], 3);
                $check[] = CalculatorComponents::getValueByParam($data['input-1'], 1);
                $check[] = CalculatorComponents::getValueByParam($data['input-4'], 4);
                $check[] = CalculatorComponents::getValueByParam($data['input-5'], 5);
                return response()->json(number_format($result, 2), 200);
            }elseif (!empty($calc) && $calc->alias == Calculator::type_ocgopo){
                $result = (
                    CalculatorComponents::getValueByParam($data['input-6'], 6) *
                    (float) CalculatorComponents::getValueByParam(8, 8)
                );
                if(!empty($data['input-7'])){
                    if($data['input-7'] == 'true'){
                        $result = $result * CalculatorComponents::getValueByParam($data['input-7'], 7);
                    }
                }
                return response()->json(number_format($result, 2), 200);
            }elseif (!empty($calc) && $calc->alias == Calculator::type_ocgpopp){
                $result = (
                    (float)CalculatorComponents::getValueByParam(14, 14) *
                    CalculatorComponents::getValueByParam($data['input-11'], 11)*
                    CalculatorComponents::getValueByParam($data['input-12'], 12)*
                    $data['input-13'] * (
                    CalculatorComponents::getValueByParam(15, 15) + CalculatorComponents::getValueByParam(16, 16)
                    )
                );
                return response()->json(number_format($result, 2), 200);
            }elseif (!empty($calc) && $calc->alias == Calculator::type_ocgopog){
                if(!empty(CalculatorComponents::getValueByParam($data['input-17'], 17)) && CalculatorComponents::getValueByParam($data['input-17'], 17) > 0){
                    $result = CalculatorComponents::getValueByParam($data['input-17'], 17) * (float) CalculatorComponents::getValueByParam(20, 20);
                }else{
                    $result = (float) CalculatorComponents::getValueByParam($data['input-18'], 18)[CalculatorComponents::getValueByParam($data['input-19'], 19)] * (float) CalculatorComponents::getValueByParam(20, 20);
                }
                return response()->json(number_format($result, 2), 200);
            }
        }
        return response()->json(number_format("0", 2), 200) ;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function callback(Request $request){
        $data = $request->all();
        if(!empty($data) && $request->isMethod('post')) {
            $callback = new CallBack();
            $callback->type = $request->input('type');
            $callback->name = $request->input('name');
            $callback->phone = $request->input('phone');
            if($request->input('type') == CallBack::TYPE_WRITE_US){
                $callback->email = $request->input('email');
                $callback->msg = $request->input('message');
            }
            $callback->status = CallBack::STATUS_NEW;
            $callback->save();
            return response()->json(['status' => 'true']);
        }
        return response()->json(['status' => 'false']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function offer(Request $request){
        $data = $request->all();
        $rules = ['captcha' => 'required|captcha'];
        $validator = Validator::make(Input::all(), $rules);
        if (!empty($data) && !$validator->fails()){
            $callback = new CallBack();
            $callback->type = CallBack::TYPE_CALCULATOR;
            $callback->name = $request->input('name');
            $callback->phone = $request->input('phone');
            $callback->msg = $request->input('message');
            $callback->status = CallBack::STATUS_NEW;
            $callback->save();
            foreach ($data as $key => $item) {
                if (strpos($key, 'input') !== false) {
                    self::addCallbackOfferData($callback->id, $key, $item);
                }
            }
           return response()->json(['status' => 'true']);
        }
        else{
            return response()->json(['status' => 'false']);
        }
    }

    private static function addCallbackOfferData($id, $key, $value)
    {
        $callbackOffer = new CallBackOffer();
        $callbackOffer->callback_id = $id;
        $callbackOffer->name = $key;
        $callbackOffer->value = $value;
        $callbackOffer->save();
    }

    public function search(Request $request){
        $query = $request->input('search');

        $queries = SearchModel::where('name', 'like', "%".$query."%")->orWhere('short_desc', 'like', "%".$query."%")->orWhere('description', 'like', "%".$query."%")->get();

        return view('search', ['query'=>$query, 'queries'=>$queries]);
    }

}