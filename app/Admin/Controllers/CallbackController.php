<?php

namespace App\Admin\Controllers;

use App\Models\CallBack;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Row;

class CallbackController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Обратные звонки');
            $content->description('список');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактировать');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить');

            $content->body($this->form());
        });
    }

    public function approve($id){
        $model = CallBack::find($id);
        if($model->status == CallBack::STATUS_NEW){
            $model->status = CallBack::STATUS_CHECK;
            $model->save();
        }
        return redirect('/admin/callback');
    }
    public function decline($id){
        $model = CallBack::find($id);
        if($model->status == CallBack::STATUS_NEW){
            $model->status = CallBack::STATUS_DECLINE;
            $model->save();
        }
        return redirect('/admin/callback');
    }


    public function view($id){

        return Admin::content(function (Content $content) use ($id) {
            $model = CallBack::find($id);

            $content->header('Обратный звонок');

            $content->breadcrumb(
                ['text' => 'Звонки', 'url' => '/callback'],
                ['text' => $model->id]
            );

            $content->row(function (Row $row) use ($id) {

                $row->column(6, function (Column $column) use ($id) {
                    $model = CallBack::find($id);

                    $column->append(view('admin.callback', ['callback'=> $model]));
                });

                $row->column(6, function (Column $column) use ($id) {
                    $column->append($this->form()->edit($id));
                });


            });

        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(CallBack::class, function (Grid $grid) {

            $grid->model()->orderBy('status', 'asc');

            $grid->disableCreateButton();

            $grid->column('type', 'Тип')->display(function () {
                $type = CallBack::getNameByType($this->type);

                return "<span class='label label-{$type['type']}'>{$type['name']}</span>";
            })->sortable();

            $grid->column('phone', 'Телефон');
            $grid->column('name', 'Имя');
            $grid->column('email', 'Почта');
            $grid->column('msg', 'Сообщение');
            $grid->column('status', 'Статус')->display(function () {
                if($this->status == CallBack::STATUS_NEW){
                    return "<span class='label label-warning'>Новый</span>";
                }
                if($this->status == CallBack::STATUS_CHECK){
                    return "<span class='label label-success'>Обработан</span>";
                }
                if($this->status == CallBack::STATUS_DECLINE){
                    return "<span class='label label-danger'>Отклонен</span>";
                }
            })->sortable();

            $grid->created_at('Создано');
            $grid->filter(function ($filter) {
                // Sets the range query for the created_at field
                $filter->between('created_at', 'Created Time')->datetime();
            });

            $grid->actions(function ($actions) {
                $actions->disableEdit();
                $actions->disableDelete();

                if(CallBack::find($actions->getKey())->status == CallBack::STATUS_NEW){
                    $actions->prepend('<a href="/admin/callback/decline/'.$actions->getKey().'"><i class="fa fa-times text-red"></i></a>');

                    $actions->prepend('<a href="/admin/callback/approve/'.$actions->getKey().'"><i class="fa fa-check"></i></a>');
                }

                if(CallBack::find($actions->getKey())->type == CallBack::TYPE_CALCULATOR){
                    $actions->prepend('<a href="/admin/callback/show/'.$actions->getKey().'"><i class="fa fa-eye text-green"></i></a>');
                }
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(CallBack::class, function (Form $form) {
            $form->disableSubmit();
            $form->disableReset();

            $form->text('name', 'Name')->readOnly();
            $form->text('phone', 'Phone')->readOnly();
            $form->text('email', 'Email')->readOnly();
            $form->text('msg', 'Massage')->readOnly();

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
