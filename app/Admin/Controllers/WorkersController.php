<?php

namespace App\Admin\Controllers;

use App\Models\CallBack;
use App\Models\Workers;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class WorkersController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Сотрудники');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактировть');
            $content->description('сотрдника');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Создать');
            $content->description('сотрудника');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Workers::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('initials', 'Инициалы');
            $grid->column('post', 'Должность');
            $grid->column('position', 'Расположение')->sortable();

            $grid->disableFilter();
            $grid->disableExport();


            $grid->created_at('Создано');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Workers::class, function (Form $form) {


            $form->text('initials', 'ФИО');

            $form->text('post', 'Должность');

            $form->text('message', 'Цитата (необязательно)');

            $form->number('position', 'Номер отображения(число)');

            $form->image('photo','Фотография')->move('/images/workers')->uniqueName();

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
