<?php

namespace App\Admin\Controllers;

use App\Models\Subsidiary;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SubsidiariesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Список филиалов');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактировать');
            $content->description('филиал');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить');
            $content->description('филиал');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Subsidiary::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->column("address",'Адрес');
            $grid->column("phone",'Телефон');
            $grid->column("email", 'Почта');
            $grid->column("lat", 'Широта');
            $grid->column('lng', 'Долгота');

            $grid->disableFilter();
            $grid->disableExport();

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Subsidiary::class, function (Form $form) {


            $form->text('address', 'Адрес')->rules('required');
            $form->text('phone', 'Телефон')->rules('required');
            $form->email('email', 'Потча')->rules('required');
            $form->text('message', 'Сообщение');

            $towns = [
                '0' => "Бишкек",
                '1' => "Ош",
                '2' => "Талас",
                '3' => "Баткен",
                '4' => "Джалал-Абад",
                '5' => "Каракол",
            ];

            $form->select('region', 'Город')->options($towns);

            $form->image('photo','Фотография')->move('/images/subsidiary')->uniqueName();

            $type = [
                Subsidiary::TYPE_MAIN =>  'Офис',
                Subsidiary::TYPE_OTHER =>  'Представитель',
            ];

            $form->select('type', 'Тип')->options($type);

            $status = [
                Subsidiary::STATUS_SHOW =>  'Показывать',
                Subsidiary::STATUS_HIDE =>  'Скрыть',
            ];

            $form->select('status', 'Статус')->options($status);

            $form->map('lat', 'lng', 'На карте');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
