<?php

namespace App\Admin\Controllers;

use App\Models\CallBack;
use App\Models\Partners;
use App\Models\PartnersGroups;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class PartnersGroupsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Группы партнеров');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Изменить');
            $content->description('изменить группу');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Создать');
            $content->description('группу');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(PartnersGroups::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('name');
            $grid->column('position');

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(PartnersGroups::class, function (Form $form) {


            $form->text('name', 'Название группы парнеров');

            $form->number('position', 'Позиция');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');

        });
    }
}
