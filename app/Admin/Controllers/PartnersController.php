<?php

namespace App\Admin\Controllers;

use App\Models\CallBack;
use App\Models\Partners;
use App\Models\PartnersGroups;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class PartnersController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Партнеры');
            $content->description('Список партнеров');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Изменить');
            $content->description('изменить пратнера');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Создать');
            $content->description('партнера');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Partners::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('name','Название партнеры');
            $grid->column('link','Ссылка');

            $grid->column('status', 'Статус')->display(function () {
                if($this->status == Partners::STATUS_SHOW){
                    return "<span class='label label-success'>Показывается</span>";
                }
                if($this->status == Partners::STATUS_HIDE){
                    return "<span class='label label-warning'>Скрыт</span>";
                }
            })->sortable();

            $grid->disableFilter();
            $grid->disableExport();

            $grid->created_at('Создано');
            $grid->updated_at('Изменено');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Partners::class, function (Form $form) {


            $form->text('name', 'Название партнера')->rules('required');

            $form->url('link', 'Ссылка')->rules('required');


            $status = [
                Partners::STATUS_SHOW =>  'Показывать',
                Partners::STATUS_HIDE =>  'Скрыть',
            ];

            $group = [];
            $modelGroup = PartnersGroups::all();
            if(!empty($modelGroup)){
                foreach ($modelGroup as $item) {
                    $group[$item->id] =  $item->name;
                }
            }

            $form->select('hasGroup.partners_group_id', 'Группа')->options($group)->rules('required');

            $form->select('status', 'Статус')->options($status);

            $form->image('logo','Логотип')->move('/images/partners')->uniqueName();

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');

        });
    }
}
