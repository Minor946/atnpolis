<?php

namespace App\Admin\Controllers;

use App\Models\Pages;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class PagesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Страницы');
            $content->description('список страниц');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактировать');
            $content->description('страницу');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Создать');
            $content->description('страницу');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Pages::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('name', 'Название');
            $grid->column('short_desc', 'Краткое описание');

            $grid->column('status', 'Статус')->display(function () {
                if($this->status == Pages::STATUS_SHOW){
                    return "<span class='label label-success'>Показывается</span>";
                }
                if($this->status == Pages::STATUS_HIDE){
                    return "<span class='label label-warning'>Скрыта</span>";
                }
            })->sortable();

            $grid->column('type', 'Тип')->display(function () {
                if($this->type == Pages::TYPE_COMMON){
                    return "<span class='label label-danger'>Обычная страница</span>";
                }
                if($this->type == Pages::TYPE_FAQ){
                    return "<span class='label label-warning'>FAQ</span>";
                }
            })->sortable();

            $grid->actions(function ($actions) {
                $actions->disableDelete();
            });

            $grid->disableFilter();
            $grid->disableExport();
            $grid->disableCreateButton();

            $grid->created_at('Создано');
            $grid->updated_at('Изменено');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Pages::class, function (Form $form) {

            $form->text('name', 'Заголовок/Название ');


            $form->textarea('short_desc', 'Короткое описание');

            $form->editor('description', 'Полное описание');

            $status = [
                Pages::STATUS_SHOW =>  'Показывать',
                Pages::STATUS_HIDE =>  'Скрыть',
            ];

            $type = [
                Pages::TYPE_COMMON =>  'Обычная',
                Pages::TYPE_FAQ =>  'FAQ',
            ];

            $form->select('status', 'Статус')->options($status);

            $form->select('type', 'Тип страницы')->options($type);

            $form->number('position', 'Позиция');

            $form->saving(function (Form $form) {
                $form->model()->alias = str_slug($form->name, '_');
                $form->model()->link = str_slug($form->name, '_');
            });

        });
    }
}
