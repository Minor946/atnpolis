<?php

namespace App\Admin\Controllers;

use App\Models\CallBack;
use App\Models\Services;
use App\Models\Sliders;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SlidersController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Слайдер');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактировать');
            $content->description('слайд');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Создать');
            $content->description('слайд');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Sliders::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column("name",'Название');
            $grid->column("description",'Описание');

            $grid->column('status', 'Статус')->display(function () {
                if($this->status == Sliders::STATUS_SHOW){
                    return "<span class='label label-success'>Показывается</span>";
                }
                if($this->status == Sliders::STATUS_HIDE){
                    return "<span class='label label-warning'>Скрыт</span>";
                }
            })->sortable();

            $grid->disableFilter();
            $grid->disableExport();

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Sliders::class, function (Form $form) {


            $form->text('name', 'Заголовок');

            $form->text('description', 'Доп описание');

            $type = [
                Sliders::TYPE_IMG =>  'Изображение',
                Sliders::TYPE_VIDEO =>  'Видео',
            ];

            $form->select('type', 'Тип')->options($type);

            $form->text('link', 'Ссылка');

            $form->file('value','Видео')->move('/sliders')->uniqueName();

            $form->image('poster','Постер')->move('/sliders/poster')->uniqueName();

            $text = [
                Sliders::TEXT_CENTER =>  'Центр',
                Sliders::TEXT_LEFT =>  'Слева',
                Sliders::TEXT_RIGHT =>  'Справа',
            ];

            $form->hidden('text_align', 'Положение текста')->options($text)->value(Sliders::TEXT_CENTER);

            $form->hidden('color', 'фон')->default('#ffffff');

            $form->hidden('top_position','Положение свреху в процентах')->value(10);
            $status = [
                Sliders::STATUS_SHOW =>  'Показывать',
                Sliders::STATUS_HIDE =>  'Скрыть',
            ];

            $form->select('status', 'Статус')->options($status);

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
