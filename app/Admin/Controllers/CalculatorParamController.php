<?php

namespace App\Admin\Controllers;

use App\Models\Calculator;

use App\Models\CalculatorParameters;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CalculatorParamController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Параметры калькулятора');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактировать');
            $content->description('параметр');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Создать');
            $content->description('Параметр');

//            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(CalculatorParameters::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->column('name', 'Название');
            $grid->column('percent','Проценты');
            $grid->column('value', 'Значение');

            $grid->column('type', 'Тип')->display(function () {
                if($this->type == CalculatorParameters::TYPE_CHECKBOX){
                    return "<span class='label label-info'>чекбокс</span>";
                }
                if($this->type == CalculatorParameters::TYPE_HIDE){
                    return "<span class='label label-success'>скрытый</span>";
                }
                if($this->type == CalculatorParameters::TYPE_NUMBER){
                    return "<span class='label label-warning'>цифры</span>";
                }
                if($this->type == CalculatorParameters::TYPE_SELECT){
                    return "<span class='label label-primary'>селект</span>";
                }
            })->sortable();

            $grid->column('placeholder','Подпись');
            $grid->column('description','Описание');
            $grid->created_at();

            $grid->disableFilter();
            $grid->disableExport();
            $grid->disableCreateButton();
            $grid->disableRowSelector();
            $grid->actions(function ($actions) {
                $actions->disableDelete();
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(CalculatorParameters::class, function (Form $form) {

            $form->text('name', 'Название');
            $form->text('percent', 'Проценты');
            $form->text('value', 'Значение');

            $form->text('placeholder', 'Подпись в поле ввода');
            $form->text('help_text', 'Подпись под полем ввода');
            $form->text('description', 'Описание параметра');
        });
    }
}
