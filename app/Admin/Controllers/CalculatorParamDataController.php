<?php

namespace App\Admin\Controllers;

use App\Models\Calculator;

use App\Models\CalculatorParameterData;
use App\Models\CalculatorParameters;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CalculatorParamDataController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Список данных параметров');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактровать');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Создать');

//            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(CalculatorParameterData::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('name', 'Название');
            $grid->column('calc_param_id', 'Параметр')->display(function () {
                return CalculatorParameters::find($this->calc_param_id)->name;
            });

            $grid->column('percent','Проценты');
            $grid->column('value', 'Значение');
            $grid->column('value_array', 'Массив значений');

            $grid->created_at();

            $grid->disableFilter();
            $grid->disableExport();
            $grid->disableCreateButton();
            $grid->disableRowSelector();
            $grid->actions(function ($actions) {
                $actions->disableDelete();
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(CalculatorParameterData::class, function (Form $form) {

            $form->text('name', 'Название');
            $form->text('percent', 'Проценты');
            $form->text('value', 'Значение');
            $form->text('value_array', 'Значение массив');

            $group = [];
            $modelParam = CalculatorParameters::all();
            if(!empty($modelParam)){
                foreach ($modelParam as $item) {
                    $group[$item->id] =  $item->name;
                }
            }

            $form->select('calc_param_id','Параметр')->options($group)->readOnly();

        });
    }
}
