<?php

namespace App\Admin\Controllers;

use App\Components\TranslateComponents;
use App\Models\Post;
use App\Models\PostAssets;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Storage;


class NewsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            // optional
             $content->header( 'Новости');

            // optional
            $content->description('список новостей:');

            // add breadcrumb since v1.5.7
            $content->breadcrumb(
                ['text' => 'Новости']
            );

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактирование');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Новости');
            $content->description('Создать новость');

            $content->breadcrumb(
                ['text' => 'Новости', 'url' => '/news'],
                ['text' => 'Создать']
            );

            $content->body($this->form());
        });
    }

    public function view($id){

        return Admin::content(function (Content $content) use ($id) {
            $model = Post::find($id);

            $content->header('Новость');
            $content->description($model->title);

            $content->breadcrumb(
                ['text' => 'Новости', 'url' => '/news'],
                ['text' => $model->title]
            );

            $content->body(view('admin.post', ['post'=>$model]));

        });
    }

    public function deleteAsset($id){
        $model = PostAssets::find($id);
        $post_id = $model->post->id;
        if($model->type == PostAssets::TYPE_GALLERY){
            Storage::delete('images/'.$model->value);
        }
        $model->delete();
        return redirect('/admin/news/show/'.$post_id);
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Post::class, function (Grid $grid) {

            $grid->disableExport();

            $grid->column('title', 'Заголовок');
            $grid->column('short_desc', 'Короткое описание');

            $grid->column('type', 'Тип')->display(function () {
                if($this->type == Post::TYPE_NEWS){
                    return "<span class='label label-info'>Новость</span>";
                }
                if($this->type == Post::TYPE_BLOG){
                    return "<span class='label label-success'>Блог</span>";
                }

            })->sortable();

            $grid->column('status', 'Статус')->display(function () {
                if($this->status == Post::STATUS_SHOW){
                    return "<span class='label label-success'>Показывается</span>";
                }
                if($this->status == Post::STATUS_HIDE){
                    return "<span class='label label-warning'>Скрыт</span>";
                }
            })->sortable();


            $grid->created_at('Создано');

            $grid->actions(function ($actions) {
                $actions->append('<a href="news/show/'.$actions->getKey().'"><i class="fa fa-eye"></i></a>');
            });

            $grid->filter(function ($filter) {

                // Sets the range query for the created_at field
                $filter->between('created_at', 'Created Time')->datetime();
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Post::class, function (Form $form) {

            $form->text('title', 'Заголовок');

            $type = [
                Post::TYPE_NEWS =>  'Новость',
                Post::TYPE_BLOG =>  'Блог',
            ];

            $form->select('type', 'Тип')->options($type);

            $form->text('short_desc', 'Краткое описание');

            $form->editor('description', 'Основное описание');

            $form->image('post_logo', 'Основное изображение')->move('/images/post')->uniqueName();



            $form->multipleFile('post_gallery', 'Галерея')->move('/images/post')->uniqueName()->removable();

            $status = [
                Post::STATUS_SHOW =>  'Показывать',
                Post::STATUS_HIDE =>  'Скрыть',
            ];

            $form->select('status', 'Статус')->options($status);

            $form->saving(function (Form $form) {
                $form->model()->alias = str_slug($form->title, '_');
                $form->model()->create_user = Admin::user()->id;;
            });

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }


}
