<?php

namespace App\Admin\Controllers;

use App\Components\TranslateComponents;
use App\Models\Translation;
use Encore\Admin\Form;
use Encore\Admin\Form\Row;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Widgets\Alert;
use Encore\Admin\Widgets\Callout;
use Encore\Admin\Widgets\InfoBox;

class TranslateController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Переводы');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактировать');
            $content->description('перевод');

            $content->body(
                $this->form($id)->edit($id)
            );
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить');
//            $content->description('description');

            $content->body($this->form());
        });
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Translation::class, function (Grid $grid) {
            $grid->disableCreateButton();

            $grid->actions(function ($actions) {
                $actions->disableDelete();
            });

            $grid->column('locale', 'Язык')->sortable();
            $grid->column('type', 'Тип')->sortable();
            $grid->column('item', 'Оригинальное название')->display(function($text) {
                return TranslateComponents::getName($this->item, $this->type, 'ru');
            });
            $grid->column('name', 'Название');


            $grid->created_at('Создано');

            $grid->disableExport();

            $grid->filter(function($filter){
                $type = [
                    'calc' => 'Калькулятор',
                    'calc_param_data'=> 'Данные параметров калькулятора',
                    'partners_group'=> 'Группы партнеров',
                    'partners' => 'Партнеры',
                    'post' => 'Новости',
                    'subsidiaries' => 'Филиалы',
                    'calc_param' => 'Параметры калькулятора',
                    'pages' => 'Страницы',
                    'service' => 'Услуги',
                    'workers' => 'Сотрудники',
                    'slider' => 'Слайдер'];

                $filter->in('locale', 'Язык')->select(['en' => 'English','kg' => 'Kyrgyz','ty' => 'Turkish']);

                $filter->in('type', 'Тип')->select($type);
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(Translation::class, function (Form $form) use ($id) {

            $model = Translation::find($id);

            $form->display('locale', 'Язык');

            $form->display('type', 'Тип');


            $modelRus = TranslateComponents::getLastByType($model->item, $model->type);

            if($model->type == TranslateComponents::TYPE_POST){
                $form->textarea('sub_item_ru', 'Доп.элемент (rus)')->default($modelRus->short_desc)->readOnly();

                $form->textarea('sub_item', 'Доп.элемент');
            }else{
                $form->display('sub_item', 'Доп.элемент');
            }

            if($model->type == TranslateComponents::TYPE_POST){
                $name = $modelRus->title;
            }elseif ($model->type == TranslateComponents::TYPE_SUBSIDIARIES){
                $name = $modelRus->address;
            }else{
                $name = $modelRus->name;
            }

            $form->text('name_ru', 'Заголовок/Название (rus)')->default($name)->readOnly();

            if ($model->type == TranslateComponents::TYPE_SUBSIDIARIES){
                $description = $modelRus->message;
            }else{
                $description = $modelRus->description;
            }

            $form->text('name', 'Заголовок/Название');

            $form->editor('description_ru', 'Описание (rus)')->default($description)->attribute(['readonly' => 'true']);

            $form->editor('description', 'Описание');

            $form->ignore(['name_ru', 'description_ru','sub_item_ru']);

        });
    }
}
