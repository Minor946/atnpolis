<?php

namespace App\Admin\Controllers;

use App\Models\CallBack;
use App\Models\Services;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Str;

class ServicesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Услуги');
            $content->description('управление услугами');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактировать');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить');
            $content->description('добавить услугу');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Services::class, function (Grid $grid) {

            $grid->id('ID')->sortable();


            $grid->column('name','Название');

            $grid->column('type', 'Тип')->display(function () {
                if($this->type == Services::TYPE_PRIVATE){
                    return "<span class='label label-info'>Частным клиентам</span>";
                }
                if($this->type == Services::TYPE_CORPORATE){
                    return "<span class='label label-primary'>Корпоративным клиентам</span>";
                }

            })->sortable();

            $grid->column('status', 'Статус')->display(function () {
                if($this->status == Services::SERVICE_SHOW){
                    return "<span class='label label-success'>Показывается</span>";
                }
                if($this->status == Services::SERVICE_HIDE){
                    return "<span class='label label-warning'>Скрыт</span>";
                }
            })->sortable();

            $grid->disableFilter();
            $grid->disableExport();

            $grid->created_at('Создано');
            $grid->updated_at('Изменено');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Services::class, function (Form $form) {

            $form->text('name', 'Название услуги');

            $form->editor('description', 'Описание');

            $type = [
                Services::TYPE_PRIVATE =>  'Частным клиентам',
                Services::TYPE_CORPORATE =>  'Корпоративным клиентам',
            ];

            $form->select('type', 'Тип')->options($type);

            $group = [];
            $modelService = Services::all();
            if(!empty($modelService)){
                foreach ($modelService as $item) {
                    $group[$item->id] =  $item->name;
                }
            }

            $form->select('parent_id','Родитель')->options($group)->rules('required');

            $form->number('position', 'Позиция');

            $status = [
                Services::SERVICE_HIDE =>  'Скрыть',
                Services::SERVICE_SHOW =>  'Показывать',
            ];

            $form->select('status', 'Статус')->options($status);

            $form->image('logo','Логотип')->move('/service')->uniqueName();

            $form->multipleFile('gallery', 'Галерея')->move('/service/gallery')->uniqueName()->removable();

            $form->saving(function (Form $form) {
                $form->model()->alias = str_slug($form->name, '_');
            });

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
