<?php

use App\Admin\Controllers\CalculatorController;
use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    Route::resource('calculator', 'CalculatorController');
    Route::resource('calculator-param', 'CalculatorParamController');
    Route::resource('calculator-param-data', 'CalculatorParamDataController');

    Route::resource('callback', 'CallbackController');
    Route::resource('services', 'ServicesController');
    Route::resource('workers', 'WorkersController');
    Route::resource('sliders', 'SlidersController');
    Route::resource('partners', 'PartnersController');
    Route::resource('partners-group', 'PartnersGroupsController');
    Route::resource('subsidiaries', 'SubsidiariesController');
    Route::resource('pages', 'PagesController');
    Route::resource('news', 'NewsController');
    Route::resource('translation', 'TranslateController');

    Route::get('callback/approve/{id}', 'CallbackController@approve');
    Route::get('callback/decline/{id}', 'CallbackController@decline');
    $router->get('/callback/show/{id}', 'CallbackController@view');
    $router->get('/news/show/{id}', 'NewsController@view');
    $router->get('/news/delete-asset/{id}', 'NewsController@deleteAsset');


    $router->get('/langman', '\Themsaid\LangmanGUI\LangmanController@index');
    $router->post('/langman/scan', '\Themsaid\LangmanGUI\LangmanController@scan');
    $router->post('/langman/save', '\Themsaid\LangmanGUI\LangmanController@save');
    $router->post('/langman/add-language', '\Themsaid\LangmanGUI\LangmanController@addLanguage');

});
