<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function()
    {
        Route::get('/', 'MainController@index');
        Route::get('/calculator', 'MainController@calculator');
        Route::get('/calculator/{type}', 'MainController@calculator');
        Route::get('/post/{alias}', 'MainController@post');
        Route::get('/service/{alias}', 'MainController@service');
        Route::get('/service/private_clients', 'MainController@service');
        Route::get('/service/corporate_clients', 'MainController@service');
        Route::get('/partners', 'MainController@partners');
        Route::get('/about', 'MainController@about');
        Route::get('/contacts', 'MainController@contacts');
        Route::get('/employees', 'MainController@employees');
        Route::get('/faq', 'MainController@faq');
        Route::get('/blog', 'MainController@blog');
        Route::get('/news', 'MainController@news');
        Route::get('/pages/{alias}', 'MainController@pages');

        Route::get('/get_captcha/{config?}', function (\Mews\Captcha\Captcha $captcha, $config = 'default') {
            return $captcha->src($config);
        });

        Route::get('/get-images/{image}', function($image = null)
        {
            $path = storage_path().'/app/images/' . $image;
            if (file_exists($path)) {
                return Response::download($path);
            }
        });
    }
);

Route::post('/get-result', 'MainController@result');
Route::post('/callback', 'MainController@callback');
Route::post('/add-offer', 'MainController@offer');
Route::get('/search', 'MainController@search');
