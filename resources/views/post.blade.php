<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/15/17
 * Time: 3:15 PM
 */

use App\Models\Post;

$post = Post::whereAlias($alias);
?>

@extends('layouts.layout')

@section('title', __('title.News :name', ['name'=> $post->title ]))

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    @include('components.post.post_body')
@endsection


@section('footer')
    @parent
@endsection


