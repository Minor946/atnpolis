<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/11/17
 * Time: 5:20 PM
 */
use App\Models\Pages;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
?>

<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9">
                    <div class="row">
                        <div class="col">
                            <h3>{{ __('messages.PrivateClients') }}</h3>
                            <?php $private_service = App\Models\Services::where('status', \App\Models\Services::SERVICE_SHOW)
                                ->where('type', \App\Models\Services::TYPE_PRIVATE)->orderBy('position')->where('parent_id', '=', null)->take(10)->get(); ?>
                            <?php if(!empty($private_service)):?>
                            <?php foreach ($private_service as $service):?>
                            <p class="footer-link">
                                <a href="{{ url('service/'.$service->alias) }}">
                                    {{\App\Components\TranslateComponents::getName($service->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                                </a>
                            </p>
                            <?php endforeach;?>
                            <?php endif;?>
                        </div>
                        <div class="col">
                            <h3>{{ __('messages.CorporateClients') }}</h3>
                            <?php $corporate_service = App\Models\Services::where('status', \App\Models\Services::SERVICE_SHOW)
                                ->where('type', \App\Models\Services::TYPE_CORPORATE)->orderBy('position')->where('parent_id', '=', null)->take(10)->get(); ?>
                            <?php if(!empty($corporate_service)):?>
                            <?php foreach ($corporate_service as $service):?>
                            <p class="footer-link">
                                <a href="{{ url('service/'.$service->alias) }}">
                                    {{\App\Components\TranslateComponents::getName($service->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                                </a>
                            </p>
                            <?php endforeach;?>
                            <?php endif;?>
                        </div>
                        <div class="col">
                            <h3>{{ __('messages.AboutCompany') }}</h3>
                            <p class="footer-link"><a href="<?php if(Route::getCurrentRoute() != null &&  Route::getCurrentRoute()->uri() == '/'):?>{{url('#news')}}<?php else:?>{{url('/news')}}<?php endif;?>">{{ __('messages.News') }}</a></p>
                            <p class="footer-link"><a href="{{ url('employees') }}">{{ __('messages.Employees') }}</a></p>
                            <p class="footer-link"><a href="{{ url('partners') }}">{{ __('messages.Partners') }}</a></p>
                            <p class="footer-link"><a href="{{ url('faq') }}">{{ __('messages.FAQ') }}</a></p>
                        </div>
                        <img  src="{{ asset('images/favicon.ico') }}"  class="bars-logo bars-footer">
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <h3>{{ __('messages.Contacts') }}</h3>
                    <p class="footer-link">
                        <a href="{{ url('contacts') }}">{{ __('messages.Subsidiary') }}</a>
                    </p>
                    <p class="footer-link">
                        <a href="mailto:{{config('email_main')}}"><i class="fa fa-envelope" aria-hidden="true"></i> {{config('email_main')}}</a><br>
                    </p>
                    <p class="footer-link">
                        <a href="tel:{{config('phone_main_draft')}}"><i class="fa fa-phone-square" aria-hidden="true"></i> {{config('phone_main')}}</a><br>
                        <span><u><a href="tel:{{config('phone_main_draft')}}">{{ __('messages.CallMe') }}</a></u> </span>
                    </p>
                    <p class="footer-link">
                        <a href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i> {{config('phone_whatsapp')}}</a><br>
                    </p>
                    <p class="footer-link">
                        <a href="tel:{{config('phone_osh_draft')}}"><i class="fa fa-phone" aria-hidden="true"></i> {{config('phone_osh')}} ({{ __('messages.Osh') }})</a>
                    </p>
                    <p class="footer-link">
                        <a href="{{config('social_link_facebook')}}" class="btn btn-social-icon btn-facebook">
                            <span class="fa fa-facebook"></span>
                        </a>
                        <a href="{{config('social_link_instagram')}}" class="btn btn-social-icon btn-instagram">
                            <span class="fa fa-instagram"></span>
                        </a>
                        <a href="{{config('social_link_google')}}" class="btn btn-social-icon btn-google">
                            <span class="fa fa-youtube"></span>
                        </a>
                        <a href="{{config('social_link_twitter')}}" class="btn btn-social-icon btn-twitter">
                            <span class="fa fa-twitter"></span>
                        </a>
                    </p>

                        <?php
                        $xml = XmlParser::load('http://www.nbkr.kg/XML/daily.xml');
                        $currency = $xml->parse([
                            'currency' => ['uses' => 'Currency[::ISOCode,Value]'],
                        ]);
                        ?>
                        <?php if(!empty($currency['currency'])):?>
                            <span>Курс по НБКР:</span>
                            <?php foreach ($currency['currency'] as $item):?>
                                <div class="currency-item">
                                    <p><?=$item['::ISOCode']?></p>
                                    <p class="currency-value"><?=$item['Value']?></p>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <p>© 2007 - <?=Carbon::today()->year?> <span>{{ __('messages.AllRightsReserved') }}</span></p>
                </div>
                <div class="col-12 col-md-3">
                    <a href="{{ url('/pages')}}/<?=Pages::find(1)->alias?>">{{ __('messages.TermsOfUse') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div id="scrollup">
        <img  src="{{ asset('/images/scrolltop.png') }}" height="60px">
    </div>

    <a href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank">
        <div class="slide_btn bg-whats-app">
            <i class="fa fa-whatsapp fa-3x" aria-hidden="true"></i>
            <div class="slide_btn_in bg-whats-app">
                {{config('phone_whatsapp')}}
            </div>
        </div>
    </a>
    <a href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank">
        <div class="slide_btn_v2 bg-commissar">
            <img src="/images/Users-Police-icon.png" style="width:  100%;">
            <div class="slide_btn_in_v2 bg-commissar">
                {{ __('modal.Commissioner') }}
            </div>
        </div>
    </a>
</footer>
