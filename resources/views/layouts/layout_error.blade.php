<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/11/17
 * Time: 5:25 PM
 */
?>
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>App Name - @yield('title')</title>
    <link rel="shortcut icon" href="{{{ asset('images/favicon2.ico') }}}">
    <!-- Fonts -->
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css">
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

</head>
<body>

@section('sidebar')
    @include('layouts.navbar')
@show
<div class="wrapper" id="wrapper">

    <div id="page-content-wrapper">
        @include('layouts.second_navbar')
        @yield('content')
    </div>
            @section('footer')
                <script src="{{ asset('js/app.js') }}"></script>
                <script src="{{ asset('js/scripts.js') }}"></script>
                <script src="{{ asset('js/swiper.min.js') }}"></script>
            @show

</div>
</body>

</html>

