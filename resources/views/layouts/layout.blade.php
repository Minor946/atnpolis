<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/11/17
 * Time: 5:25 PM
 */

use Illuminate\Support\Facades\Route;
$route = "";

if(!empty(Route::getFacadeRoot()->current())){
    $route = Route::getFacadeRoot()->current()->uri();
}
$mainStyle = "";
?>
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>АТН ПОЛИС - @yield('title')</title>
    <link rel="shortcut icon" href="{{{ asset('images/favicon3.ico') }}}">
    <!-- Fonts -->
    <!-- Styles -->
    <link href="{{ url('/css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/css/all.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/css/responsive.css') }}" rel="stylesheet" type="text/css">
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>

</head>
<body>

@section('sidebar')
    @include('layouts.navbar')
    @include('layouts.second_navbar')
@show

<div class="wrapper" id="wrapper">
    <?php if($route == "/" || $route == "en"|| $route == "ky"|| $route == "ty"|| $route == "ru"){
      $mainStyle = "style = 'padding-top: 0;'";
    }
    ?>
    <div id="page-content-wrapper" <?=$mainStyle?> >

        @section('menu')
            @include('layouts.menu')
        @show

        @yield('content')
    </div>

    @section('footer')
    @include('layouts.footer')
        <script src="{{ url('js/app.js') }}"></script>
        <script src="{{ url('js/scripts.js') }}"></script>
        <script src="{{ url('js/swiper.min.js') }}"></script>
        <script src="{{ url('js/notie.min.js') }}"></script>
    @show
</div>

@include('modals.modal_recall')
@include('modals.modal_commissioner')
@include('modals.modal_specialist')


</body>

</html>

