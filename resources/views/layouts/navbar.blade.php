<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/11/17
 * Time: 5:20 PM
 */
?>

<nav id="navbar-first"  class="navbar navbar-expand-lg navbar-light fixed-top bg-white navbar-shadow">
    <div class="row" style="width: 100%;">
        <div class="col-md-4 mobile-hide">
            <div class="hamburger hamburger--arrowalt" id="menu-toggle"  aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
            <div class="form-group form-inline form-navbar">
                {{ Form::open(array ('url'=>'/search', 'method' => 'get', 'id'=>'animated')) }}
                    <i class="fa fa-search" aria-hidden="true"></i>
                        {{ Form::text("search", null, array_merge(['class' => 'form-control', 'id'=>'search-form', 'required', 'placeholder'=>__('messages.SearchPlaceholder')])) }}
                    <input type="submit" class="hide"/>
                </form>
            </div>
        </div>

        <div class="col-12 col-md-4" align="center">
            <a href="{{ url('/') }}">
                <img  src="{{ asset('images/logo-color.png') }}" height="58px">
            </a>
        </div>
        <div class="col-md-4">
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                   <div class="col-8">
                       <div class="navbar-contact">
                           <div class="navbar-item-contact">
                               <i class="fa fa-phone" aria-hidden="true"></i>
                               <a href="tel:{{config('phone_bishkek_draft')}}">{{config('phone_bishkek')}} ({{ __('messages.Bishkek') }})</a>
                           </div>
                           <div class="navbar-item-contact">
                               <i class="fa fa-phone" aria-hidden="true"></i>
                               <a href="tel:{{config('phone_osh_draft')}}">{{config('phone_osh')}} ({{ __('messages.Osh') }})</a>
                           </div>
                           <div class="navbar-item-contact">
                               <i class="fa fa-whatsapp" aria-hidden="true"></i>
                               <a href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank"> {{config('phone_whatsapp')}}</a><br>
                           </div>
                       </div>
                   </div>
                   <div class="col-4">
                       <div class="row row-lang">
                           <div class="col">
                               @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                   <?php $checkLocal = false;  if(App::getLocale() == $localeCode) $checkLocal = true; ?>
                                   <a rel="alternate" hreflang="{{ $localeCode }}" @if($checkLocal) class="active" @endif href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">{{$properties['short']}}</a>
                               @endforeach
                           </div>
                       </div>
                       <div class="navbar-callback trigger-recall-modal">
                           <a class="btn btn-warning text-white" href="javascript:void(0);">{{ __('messages.Callback') }}</a>
                       </div>
                   </div>
            </div>
        </div>
    </div>
</nav>