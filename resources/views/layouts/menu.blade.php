<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/11/17
 * Time: 6:15 PM
 */

use App\Models\Pages;
use Illuminate\Support\Facades\Route;
$classMenu = "";
?>
<!-- Sidebar -->
    <div class="navigation hide" id="navigation">
        <ul>
            <li class="has-sub">
                <a href="javascript:void(0);">{{ __('messages.AboutCompany') }}</a>
                <ul>
                    <li><a class="sub-link" href="{{ url('/about') }}">{{ __('messages.AboutCompany') }}</a></li>
                    <li><a class="sub-link" href="{{ url('/employees') }}">{{ __('messages.Employees') }}</a></li>
                    <li><a class="sub-link" href="{{ url('/pages')}}/<?=Pages::find(3)->alias?>"><?=Pages::find(3)->name?></a></li>
                    <li><a class="sub-link" href="{{ url('/about#license') }}">{{ __('messages.License') }}</a></li>
                    <li><a class="sub-link" href="{{ url('/pages')}}/<?=Pages::find(4)->alias?>"><?=Pages::find(4)->name?></a></li>
                    <li><a class="sub-link" href="{{ url('/pages')}}/<?=Pages::find(5)->alias?>"><?=Pages::find(5)->name?></a></li>
                    <li><a class="sub-link" href="{{ url('/contacts') }}">{{ __('messages.Contacts') }}</a></li>
                </ul>
            </li>

            <li class="has-sub">
                <a href="javascript:void(0);">{{ __('messages.PrivateClients') }}</a>
                <ul>
                    <?php $private_service = App\Models\Services::where('status', \App\Models\Services::SERVICE_SHOW)->where('parent_id', '=', null)
                        ->where('type', \App\Models\Services::TYPE_PRIVATE)->orderBy('position')->take(10)->get(); ?>
                    <?php if(!empty($private_service)):?>
                    <?php foreach ($private_service as $service):?>
                        <?php $has_child = App\Models\Services::where('parent_id', $service->id)->take(10)->get(); ?>
                        <?php if(!empty($has_child)&& count($has_child) > 0):?>
                        <li class="has-sub">
                            <a class="sub-link" href="{{ url('/service/'.$service->alias) }}">
                                {{\App\Components\TranslateComponents::getName($service->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                            </a>
                            <ul>
                                <?php foreach ($has_child as $child):?>
                                <li>
                                    <a class="sub-link" href="{{ url('/service/'.$child->alias) }}">
                                        {{\App\Components\TranslateComponents::getName($child->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                                    </a>
                                </li>
                                <?php endforeach;?>
                            </ul>
                        </li>
                        <?php else:?>
                        <li>
                            <a class="sub-link" href="{{ url('/service/'.$service->alias) }}">
                                {{\App\Components\TranslateComponents::getName($service->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                            </a>
                        </li>
                        <?php endif;?>
                    <?php endforeach;?>
                    <?php endif;?>
                </ul>
            </li>

            <li class="has-sub">
                <a href="javascript:void(0);">{{ __('messages.CorporateClients') }}</a>
                <ul>
                    <?php $corporate_service = App\Models\Services::where('status', \App\Models\Services::SERVICE_SHOW)->where('parent_id', '=', null)
                        ->where('type', \App\Models\Services::TYPE_CORPORATE)->orderBy('position')->take(10)->get(); ?>
                    <?php if(!empty($corporate_service)):?>
                    <?php foreach ($corporate_service as $service):?>
                    <?php $has_child = App\Models\Services::where('parent_id', $service->id)->get(); ?>
                    <?php if(!empty($has_child)&& count($has_child) > 0):?>
                    <li class="has-sub">
                        <a class="sub-link" href="{{ url('/service/'.$service->alias) }}">
                            {{\App\Components\TranslateComponents::getName($service->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                        </a>
                        <?php if($service->id == 11):?>
                            <?php $classMenu = "sub-menu-top";?>
                        <?php endif;?>
                        <ul class="<?=$classMenu?>">
                            <?php foreach ($has_child as $child):?>
                            <li>
                                <a class="sub-link" href="{{ url('/service/'.$child->alias) }}">
                                    {{\App\Components\TranslateComponents::getName($child->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                                </a>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </li>
                    <?php else:?>
                    <li>
                        <a class="sub-link" href="{{ url('/service/'.$service->alias) }}">
                            {{\App\Components\TranslateComponents::getName($service->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                        </a>
                    </li>
                    <?php endif;?>
                    <?php endforeach;?>
                    <?php endif;?>
                </ul>
            </li>

            <li class="has-sub">
                <a href="{{ url('calculator') }}">{{ __('messages.Calculator') }}</a>
                <ul>
                    <?php $calcs_type = App\Models\Calculator::where('status', \App\Models\Calculator::CALC_SHOW)->orderBy('position')->take(10)->get(); ?>
                    <?php if(!empty($calcs_type)):?>
                    <?php foreach ($calcs_type as $calc_type):?>
                    <li><a  class="sub-link" href="{{ url('/calculator/'.$calc_type->alias) }}"> {{\App\Components\TranslateComponents::getName($calc_type->id, \App\Components\TranslateComponents::TYPE_CALC, LaravelLocalization::getCurrentLocale() )}}</a></li>
                    <?php endforeach;?>
                    <?php endif;?>
                        {{--<li><a class="sub-link" href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank">{{ __('messages.WhatsAppConsult') }}</a></li>--}}
                        {{--<li><a class="sub-link" href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank">{{ __('messages.ConsultaionEmergencyCommissioner') }}</a></li>--}}
                </ul>
            </li>

            <li>
                <a href="<?php if(Route::getCurrentRoute() != null && Route::getCurrentRoute()->uri() == '/'):?>{{url('#news')}}<?php else:?>{{url('/news')}}<?php endif;?>">{{ __('messages.News') }}</a>
            </li>

            <li class="has-sub">
                <a href="javascript:void(0);">{{ __('messages.reinsurance') }}</a>
                <ul>
                    <li><a class="sub-link" href="{{ url('/pages')}}/<?=Pages::find(10)->alias?>"><?=Pages::find(10)->name?></a></li>
                    <li><a  class="sub-link" href="{{ url('/partners/#partners3') }}">{{\App\Components\TranslateComponents::getName(3, \App\Components\TranslateComponents::TYPE_PARTNERS_GROUP_NAME, LaravelLocalization::getCurrentLocale() )}}</a></li>
                </ul>
            </li>

            <li class="has-sub">
                <a href="javascript:void(0);">{{ __('messages.Consultation') }}</a>
                <ul>
                    <li><a class="sub-link" href="{{ url('/pages')}}/<?=Pages::find(2)->alias?>">{{ __('messages.InsureCase') }}</a></li>
                    <li><a class="sub-link"  href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank" >{{ __('action.Call') }} {{ __('action.ASpecialist') }}</a></li>
                    <li><a class="sub-link" href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank">{{ __('messages.WhatsAppConsult') }}</a></li>
                    <li><a class="sub-link"  href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank" >{{ __('action.CallOfTheEmergencyCommissioner') }}</a></li>
                </ul>
            </li>

            <li><a href="{{ url('/blog') }}">{{ __('messages.Blog') }}</a></li>

            <li class="has-sub">
                <a href="{{ url('/partners') }}">{{ __('messages.Partners') }}</a>
                <ul>
                    <?php $groups = App\Models\PartnersGroups::all()->sortBy("position"); ?>
                    <?php if(!empty($groups)):?>
                    <?php foreach ($groups as $group):?>
                    <?php $partners_list = \App\Models\PartnersHasGroups::where('partners_group_id', $group->id)->get();?>
                    <?php if(count($partners_list) > 0):?>
                    <li><a  class="sub-link" href="{{ url('/partners/#partners'.$group->id) }}">{{\App\Components\TranslateComponents::getName($group->id, \App\Components\TranslateComponents::TYPE_PARTNERS_GROUP_NAME, LaravelLocalization::getCurrentLocale() )}}</a></li>
                    <?php endif;?>
                    <?php endforeach;?>
                    <li><a class="sub-link" href="{{ url('/pages')}}/<?=Pages::find(13)->alias?>"><?=Pages::find(13)->name?></a></li>
                    <li><a class="sub-link" href="{{ url('/pages')}}/<?=Pages::find(12)->alias?>"><?=Pages::find(12)->name?></a></li>
                    <li><a class="sub-link" href="{{ url('/pages')}}/<?=Pages::find(11)->alias?>"><?=Pages::find(11)->name?></a></li>
                    <?php endif;?>
                </ul>
            </li>

            <li class="has-sub">
                <a href="javascript:void(0);">{{ __('messages.FAQ') }}</a>
                <ul>
                    <?php $faqPage = \App\Models\Pages::where('type', \App\Models\Pages::TYPE_FAQ)->orderBy('position', 'asc')->get();?>
                    <?php if(!empty($faqPage)):?>
                    <?php foreach ($faqPage as $page):?>
                    <li><a class="sub-link" href="/faq#<?=$page->alias?>"><?=$page->name?></a></li>
                    <?php endforeach;?>
                    <?php endif;?>
                </ul>
            </li>
        </ul>
        <img src="/images/brand_block.png" class="brand-block hide">
    </div>
</ul>