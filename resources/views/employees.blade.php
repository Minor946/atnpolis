<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/15/17
 * Time: 3:40 PM
 */
?>

@extends('layouts.layout')

@section('title', __('title.Employees'))

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    @include('components.employees.employees_body')
@endsection


@section('footer')
    @parent
@endsection

