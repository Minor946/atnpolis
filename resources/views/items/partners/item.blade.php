<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/11/18
 * Time: 5:22 PM
 */

$partner = \App\Models\Partners::find($partner_id);

?>
<?php if($partner->status == \App\Models\Partners::STATUS_SHOW):?>
<div class="partners-item">
    <div class="row">
        <div class="col-md-7">
            <a href="{{$partner->link}}">
            <?php if(empty($partner->logo)):?>
                <img src="http://via.placeholder.com/270x100" width="100%" height="100" class="img-contain">
            <?php else:?>
                <img src="/uploads/{{$partner->logo}}" width="100%" height="100" class="img-contain">
            <?php endif;?>
            </a>
        </div>
        <div class="col-md-5">
            <div>{{\App\Components\TranslateComponents::getName($partner->id, \App\Components\TranslateComponents::TYPE_PARTNERS_NAME, LaravelLocalization::getCurrentLocale() )}}</div>
            <div class="clearfix"></div>
            <p><a href="{{$partner->link}}">{{ __('messages.Url')}}</a></p>
        </div>
    </div>
</div>
<?php endif;?>
