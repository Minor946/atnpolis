<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/13/17
 * Time: 5:29 PM
 */

use Carbon\Carbon;

$dt = Carbon::parse($post->created_at);
?>

<a class="post-link" href="{{ url('post/'.$post->alias) }}">
    <div class="post">
        <div class="post-body-item">
            <div class="post-image">
                <img src="{{ asset('/get-images/post_logo_'.$post->id.'.jpg') }}" class="float-left post-img" alt="">
            </div>
            <div class="post-data">
                <h3>{{\App\Components\TranslateComponents::getName($post->id, \App\Components\TranslateComponents::TYPE_POST, LaravelLocalization::getCurrentLocale() )}}</h3>
                <p>{{\App\Components\TranslateComponents::getPostShortDesc($post->id, LaravelLocalization::getCurrentLocale() )}}</p>
                <span>{{ $dt->toDateString() }}</span>
            </div>
        </div>
    </div>
</a>