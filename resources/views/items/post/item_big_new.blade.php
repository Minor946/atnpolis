<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 3/2/18
 * Time: 2:09 PM
 */


use Carbon\Carbon;
Carbon::setLocale(app()->getLocale());

$dt = Carbon::parse($post->created_at);

?>

<a href="{{ url('post/'.$post->alias) }}" class="post-card-link">
    <div class="post-card-big">
        <img class="post-card-big-img" src="{{ asset('/uploads/'.$post->post_logo) }}">
        <div class="post-type <?= \App\Models\Post::getColor($post->type)?>">
            <?php if($post->type == \App\Models\Post::TYPE_NEWS):?>
            {{__("post.News")}}
            <?php else:?>
            {{__("post.blog")}}
            <?php endif;?>
        </div>
        <div class="post-card-big-overlay" >
           <div class="post-card-big-title-bg">
               <h4 class="post-card-big-title">{{\App\Components\TranslateComponents::getName($post->id, \App\Components\TranslateComponents::TYPE_POST, LaravelLocalization::getCurrentLocale() )}}</h4>
           </div>
           <div class="padding-5">
               <p class="post-card-big-desc text-muted">{{\App\Components\TranslateComponents::getPostShortDesc($post->id, LaravelLocalization::getCurrentLocale() )}}</p>
               <div>
                   <div class="post-card-big-more" align="left">{{__("messages.Full")}}</div>
                   <div class="post-card-big-date" align="right">{{ $dt->toDateString() }}</div>
               </div>
           </div>
        </div>
    </div>
</a>