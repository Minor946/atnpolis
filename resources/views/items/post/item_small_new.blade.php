<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 3/2/18
 * Time: 2:30 PM
 */

use Carbon\Carbon;

$dt = Carbon::parse($post->created_at);

?>

<a href="{{ url('post/'.$post->alias) }}" class="post-card-link">
    <div class="post-card-big">
        <img class="post-card-small-img" src="{{ asset('/uploads/'.$post->post_logo) }}">
        <div class="post-type <?= \App\Models\Post::getColor($post->type)?>">
            <?php if($post->type == \App\Models\Post::TYPE_NEWS):?>
                {{__("post.News")}}
            <?php else:?>
                {{__("post.blog")}}
            <?php endif;?>
        </div>
        <div class="post-card-small-overlay" >
                <h4 class="post-card-small-title">{{\App\Components\TranslateComponents::getName($post->id, \App\Components\TranslateComponents::TYPE_POST, LaravelLocalization::getCurrentLocale() )}}</h4>
                <p class="post-card-small-desc text-muted">{{\App\Components\TranslateComponents::getPostShortDesc($post->id, LaravelLocalization::getCurrentLocale() )}}</p>
            <div class="post-gradient-bg"></div>
            <div align="right">
                    <p class="post-card-small-date">{{ $dt->toDateString() }}</p>
            </div>
        </div>
    </div>
</a>