<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 3/7/18
 * Time: 1:25 PM
 */
?>
@if ($type === 'service')
    <?php
    $name = \App\Components\TranslateComponents::getName($item->item, \App\Components\TranslateComponents::TYPE_SERVICE, app()->getLocale() );
    $desc = \App\Components\TranslateComponents::getDesc($item->item, \App\Components\TranslateComponents::TYPE_SERVICE, app()->getLocale() );
    $url = "/service/".$item->alias;
    $label = "<span class='badge badge-primary'>".__('messages.Services')."</span>";
    ?>
@elseif($type === "post")
    <?php
    $name = \App\Components\TranslateComponents::getName($item->item, \App\Components\TranslateComponents::TYPE_POST, app()->getLocale() );
    $desc = \App\Components\TranslateComponents::getDesc($item->item, \App\Components\TranslateComponents::TYPE_POST, app()->getLocale() );
    $url = "/post/".$item->alias;
    $label = "<span class='badge badge-success'>".__('messages.News')."</span>";
    ?>
@elseif($type === "calc")
    <?php
    $name = \App\Components\TranslateComponents::getName($item->item, \App\Components\TranslateComponents::TYPE_CALC, app()->getLocale() );
    $desc = \App\Components\TranslateComponents::getDesc($item->item, \App\Components\TranslateComponents::TYPE_CALC, app()->getLocale() );
    $url = "/calculator/".$item->alias;
    $label = "<span class='badge badge-info'>".__('messages.Calculator')."</span>";
    ?>
@elseif($type === "pages")
    <?php
    $name = \App\Components\TranslateComponents::getName($item->item, \App\Components\TranslateComponents::TYPE_PAGES, app()->getLocale() );
    $desc = \App\Components\TranslateComponents::getDesc($item->item, \App\Components\TranslateComponents::TYPE_PAGES, app()->getLocale() );
    $url = "/pages/".$item->alias;
    $label = "<span class='badge badge-primary'>".__('messages.AboutCompany')."</span>";
    ?>
@endif

<div class="search-item">
    <div class="search-name">
        <a href="{{url($url)}}">
            {{$name}}
        </a><?=$label;?>
    </div>
    <div class="search-desc">
        <p class="text-muted">
            {{strip_tags($desc)}}
        </p>
    </div>
</div>