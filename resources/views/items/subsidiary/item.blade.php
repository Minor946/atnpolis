<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/12/18
 * Time: 3:03 PM
 */


?>

<div class="subsidiary-item">
    <div class="row">
        <div class="col-md-3" align="center">
            <?php if($subsidiary->type == \App\Models\Subsidiary::TYPE_MAIN):?>
                <img class="subsidiary-img" src="{{ asset('/uploads/'.$subsidiary->photo) }}" >
            <?php else:?>
                <img class="subsidiary-img" src="/images/stub.png" >
            <?php endif;?>
        </div>
        <div class="col-md-3">
            <div class="subsidiary-info">
                <p class="subsidiary-address">
                    {{\App\Components\TranslateComponents::getName($subsidiary->id, \App\Components\TranslateComponents::TYPE_SUBSIDIARIES, LaravelLocalization::getCurrentLocale() )}}
                </p>
                <p>тел: {{$subsidiary->phone}}</p>
                <p>email: <a href="mailto:{{$subsidiary->email}}">{{$subsidiary->email}}</a></p>
                <?php if(!empty($subsidiary->message)):?>
                    <p>
                        {{\App\Components\TranslateComponents::getDesc($subsidiary->id, \App\Components\TranslateComponents::TYPE_SUBSIDIARIES, LaravelLocalization::getCurrentLocale() )}}
                    </p>
                <?php endif;?>

            </div>
        </div>
        <div class="col-md-6">
            <div id="map{{$subsidiary->id}}" class="map"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var myMap;
    ymaps.ready(init);

    function init () {
        myMap = new ymaps.Map('map{{$subsidiary->id}}', {
            center: [{{$subsidiary->lat}} , {{$subsidiary->lng}}],
            zoom: 13,
            controls: ['zoomControl']
        });

        myMap.geoObjects
            .add(new ymaps.Placemark([{{$subsidiary->lat}} , {{$subsidiary->lng}}], {
            }, {
                preset: 'islands#icon',
                iconColor: '#b6301a'
            }));
    }
</script>