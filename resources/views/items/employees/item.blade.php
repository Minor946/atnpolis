<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/15/18
 * Time: 1:23 PM
 */
?>
<li class="cards__item_big">
    <div class="card-v2">
        <?php if(!empty($worker->photo)):?>
            <div class="card__image without-filter" style="background-image: url(/uploads/{{$worker->photo}});"></div>
            <?php else:?>
            <div class="card__image without-filter" style="background-image: url(/images/no_user.jpg);"></div>
        <?php endif;?>
        <div class="card__content card__worker">
            <div class="card__title">
                <h5>{{\App\Components\TranslateComponents::getName($worker->id, \App\Components\TranslateComponents::TYPE_WORKERS, LaravelLocalization::getCurrentLocale() )}}</h5>
                <p class="text-muted">{{\App\Components\TranslateComponents::getDesc($worker->id, \App\Components\TranslateComponents::TYPE_WORKERS, LaravelLocalization::getCurrentLocale() )}}</p>
            </div>
        </div>
    </div>
</li>
