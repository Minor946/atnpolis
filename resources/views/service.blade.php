<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/15/17
 * Time: 3:17 PM
 */

use App\Models\Services;

$service = Services::whereAlias($alias);
?>

@extends('layouts.layout')

@section('title', __('title.Services :name'))

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    @include('components.service.service_body')
@endsection


@section('footer')
    @parent
@endsection

