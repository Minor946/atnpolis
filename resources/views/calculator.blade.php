<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/15/17
 * Time: 3:13 PM
 */

$calc = \App\Models\Calculator::whereType($type);
?>
@extends('layouts.layout')

@section('title', __('title.Calculator :name', ['name'=> App\Components\CalculatorComponents::getSelectedName($type,LaravelLocalization::getCurrentLocale()) ]))

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    @include('components.calculator.calculator_body')
@endsection


@section('footer')
    @parent
@endsection
