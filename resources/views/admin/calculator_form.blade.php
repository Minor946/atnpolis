<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/8/18
 * Time: 4:35 PM
 */

use App\Models\CalculatorParameters;
use App\Models\CalculatorParameterData;

?>

<?php $params = CalculatorParameters::paramByType($type); ?>
@if(!empty($params))
    {{ Form::open(array ('url'=>'#', 'method' => 'post', 'id'=>'form-calc-result')) }}
    <input type="text" class="form-control hide" name="input-type" hidden value="{{$type}}">
    @foreach($params as $param)
        <?php $detail_param = CalculatorParameters::find($param->calc_param_id); ?>
        <?php $value =  \App\Models\CallBackOffer::getType('input-'.$detail_param->id, $callback_id);?>
        @if($detail_param->type == \App\Models\CalculatorParameters::TYPE_SELECT )
            <div class="form-group row">
                <div class="col-md-4">
                    <label>
                        {{\App\Components\TranslateComponents::getName($detail_param->id, \App\Components\TranslateComponents::TYPE_CALC_PARAM, LaravelLocalization::getCurrentLocale() )}}
                    </label>
                </div>
                <div class="col-md-8">
                    <select class="js-example-basic-single js-states form-control" name="input-{{$detail_param->id}}">

                        <?php $param_dates = CalculatorParameterData::where('calc_param_id', $detail_param->id)->get(); ?>
                        <?php if(!empty($param_dates)):?>
                        <?php foreach ($param_dates as $param_data):?>
                        <?php if($value == $param_data->id):?>
                            <option value="<?=$param_data->id?>" selected>{{\App\Components\TranslateComponents::getName($param_data->id, \App\Components\TranslateComponents::TYPE_CALC_PARAM_DATA, LaravelLocalization::getCurrentLocale() )}}</option>
                            <?php else:?>
                            <option value="<?=$param_data->id?>">{{\App\Components\TranslateComponents::getName($param_data->id, \App\Components\TranslateComponents::TYPE_CALC_PARAM_DATA, LaravelLocalization::getCurrentLocale() )}}</option>
                        <?php endif;?>
                        <?php endforeach;?>
                        <?php endif;?>
                    </select>
                </div>
            </div>
        @elseif($detail_param->type == \App\Models\CalculatorParameters::TYPE_NUMBER)
            <div class="form-group row">
                <div class="col-md-4">
                    <label>
                        {{\App\Components\TranslateComponents::getName($detail_param->id, \App\Components\TranslateComponents::TYPE_CALC_PARAM, LaravelLocalization::getCurrentLocale() )}}
                    </label>
                </div>
                <div class="col-md-8">
                    <input type="number" value="{{$value}}" class="form-control" placeholder="{{\App\Components\TranslateComponents::getPlaceholder($detail_param->id, LaravelLocalization::getCurrentLocale() )}}" min="0" name="input-{{$detail_param->id}}">
                    <span class="text-help">
                        {{\App\Components\TranslateComponents::getHelpText($detail_param->id, LaravelLocalization::getCurrentLocale() )}}
                    </span>
                </div>
            </div>
        @elseif($detail_param->type == \App\Models\CalculatorParameters::TYPE_CHECKBOX)
            <div class="form-check">
                <?php if(!empty($value)):?>
                        <input class="form-check-input" type="checkbox" value="true" checked name="input-{{$detail_param->id}}" id="input-{{$detail_param->id}}">
                    <?php else:?>
                        <input class="form-check-input" type="checkbox" value="true" name="input-{{$detail_param->id}}" id="input-{{$detail_param->id}}">
                <?php endif;?>
                <label class="form-check-label" for="input-{{$detail_param->id}}">
                    {{\App\Components\TranslateComponents::getName($detail_param->id, \App\Components\TranslateComponents::TYPE_CALC_PARAM, LaravelLocalization::getCurrentLocale() )}}
                </label>
            </div>
        @endif
    @endforeach
    {{ Form::close() }}
@endif

<script>
    $(document).ready(function($) {
        getResultCalc();


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#form-calc-result input[type=number]').each(function(){
            $(this).change(function() {
                getResultCalc();
            });
        });

        $('#form-calc-result input[type=checkbox]').each(function(){
            $(this).change(function() {
                getResultCalc();
            });
        });

        $('#form-calc-result select').each(function(){
            $(this).select2({
                minimumResultsForSearch: -1,
                theme: "default"
            }).on('select2:select', function (e) {
                getResultCalc();
            });
        });

        function getResultCalc() {
            $.ajax({
                method: 'post',
                url: '/get-result',
                data : $("#form-calc-result").serializeArray(),
                success: function (html) {
                    $("#premium-price").html(html);
                }
            });
        }
    });
</script>