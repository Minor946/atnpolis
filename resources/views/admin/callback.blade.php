<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 3/6/18
 * Time: 3:50 PM
 */

$offers = $callback->offer()->get();

$type = \App\Models\CallBackOffer::getType('input-type', $callback->id);
?>

<div class="container box box-success">
    <div class="box-body">
        @include('admin.calculator_form',['type' => $type,'callback_id'=>$callback->id])
    </div>

    <hr class="hr-light">
    <div class="block-premium row">
        <div class="col-md-3 label-premium">
            <small>{{ __('messages.Award') }} <br> {{ __('messages.ToPay') }}  </small>
        </div>
        <div class="col-md-9"><h2 id="premium-price">0.00</h2></div>
    </div>

    <div class="row" align="center">
        <a href="/admin/callback/approve/<?=$callback->id?>"><i class="fa fa-check fa-3x text-greenф"></i></a>

        <a href="/admin/callback/decline/<?=$callback->id?>"><i class="fa fa-times fa-3x text-red"></i></a>
    </div>
</div>

