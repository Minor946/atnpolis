<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 2/27/18
 * Time: 1:18 PM
 */
?>
<div class="modal-wrapper" id="modal-specialist">
    <div class="modal">
        <div class="head head-primary">
            <h2 class="head-title">{{ __('messages.Callback') }}</h2>
            <a class="btn-close trigger-specialist-modal" href="javascript:void(0);">
                <i class="fa fa-times" aria-hidden="true"></i>
            </a>
        </div>
        <div class="content">
            <div class="row">
                <div class="col">
                    <p>{{ __('modal.You can contact our specialists by these numbers') }}</p>
                    <ul>
                        <li><h5>{{config('specialist_number_1')}}</h5></li>
                        <li><h5>{{config('specialist_number_2')}}</h5></li>
                        <li><h5>{{config('specialist_number_3')}}</h5></li>
                    </ul>
                    <p>{{ __('modal.or leave your data and our specialists will call you back at the earliest possible date') }}</p>
                </div>
            </div>
            <div class="row margin-top-30">
                <div class="col">
                    {!! Form::open(['url' => '/callback','id'=>'form-specialist']) !!}
                    {{Form::token()}}
                    <div class="form-group">
                        {{ Form::label( __('messages.Name'), null, ['class' => 'control-label']) }}
                        {{ Form::text("name", "", array_merge(['class' => 'form-control', 'id'=>'specialist-name', 'placeholder'=>__('messages.Name')])) }}
                    </div>

                    {{ Form::hidden('type', \App\Models\CallBack::TYPE_SPECIALIST)}}
                    <div class="form-group">
                        {{ Form::label(__('messages.Phone'), null, ['class' => 'control-label required']) }}
                        {{ Form::text("phone", "", array_merge(['class' => 'form-control', 'required'=>true, 'id'=>'specialist-phone', 'placeholder'=>__('messages.Name')])) }}
                    </div>

                    <div align="center">
                        {{Form::submit(__('messages.Offer'),['class' => 'btn btn-success btn-submit'])}}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function($){
        $('#specialist-phone').mask('(000) 00-00-00', {placeholder: "(000) 00-00-00", clearIfNotMatch: true});

        $('#form-specialist').on('submit', function(e){
            $.ajax({
                type		: "POST",
                url		    : "/callback",
                data		: $("#form-specialist").serializeArray(),
                cache		: false,
                success		: function(html) {
                    if(html['status']==="true"){
                        notie.alert({
                            type: 'success',
                            text: "{{__('modal.Success')}}",
                            stay: false,
                            time: 6,
                            position: 'bottom'
                        })
                    }
                    $('#modal-specialist').toggleClass('open');
                    $('.wrapper').toggleClass('blur-it');
                }
            });
            e.preventDefault();
        });
    });
</script>
