<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 2/27/18
 * Time: 1:05 PM
 */
?>

<div class="modal-wrapper" id="modal-commissioner">
    <div class="modal">
        <div class="head">
            <h2 class="head-title">{{ __('action.CallOfTheEmergencyCommissioner') }}</h2>
            <a class="btn-close trigger-commissioner-modal" href="javascript:void(0);">
                <i class="fa fa-times" aria-hidden="true"></i>
            </a>
        </div>
        <div class="content">
            <div class="row">
                <div class="col">
                    <p>{{ __('modal.Emergency commissioner') }} {{config('commissioner_name')}}</p>
                    <ul>
                        <li><h5>{{config('commissioner_phone')}}</h5></li>
                    </ul>
                </div>
            </div>
            <div class="row margin-top-30">
                <div class="col">
                    {!! Form::open(['url' => '/callback','id'=>'form-commissioner']) !!}
                    {{Form::token()}}
                    <div class="form-group">
                        {{ Form::label( __('messages.Name'), null, ['class' => 'control-label']) }}
                        {{ Form::text("name", "", array_merge(['class' => 'form-control', 'id'=>'commissioner-name', 'placeholder'=>__('messages.Name')])) }}
                    </div>

                    {{ Form::hidden('type', \App\Models\CallBack::TYPE_COMMISSIONER)}}
                    <div class="form-group">
                        {{ Form::label(__('messages.Phone'), null, ['class' => 'control-label required']) }}
                        {{ Form::text("phone", "", array_merge(['class' => 'form-control', 'required'=>true, 'id'=>'commissioner-phone', 'placeholder'=>__('messages.Name')])) }}
                    </div>

                    <div align="center">
                        {{Form::submit(__('messages.Offer'),['class' => 'btn btn-success btn-submit'])}}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function($){
        $('#commissioner-phone').mask('(000) 00-00-00', {placeholder: "(000) 00-00-00", clearIfNotMatch: true});

        $('#form-commissioner').on('submit', function(e){
            $.ajax({
                type		: "POST",
                url		    : "/callback",
                data		: $("#form-commissioner").serializeArray(),
                cache		: false,
                success		: function(html) {
                    if(html['status']==="true"){
                        notie.alert({
                            type: 'success',
                            text: "{{__('modal.Success')}}",
                            stay: false,
                            time: 6,
                            position: 'bottom'
                        })
                    }
                    $('#modal-commissioner').toggleClass('open');
                    $('.wrapper').toggleClass('blur-it');
                }
            });
            e.preventDefault();
        });
    });
</script>
