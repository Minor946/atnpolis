<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/15/17
 * Time: 3:15 PM
 */
?>

@extends('layouts.layout')

@section('title', __('title.Partners'))

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    @include('components.partners.partners_body')
@endsection

@section('footer')
    @parent
@endsection