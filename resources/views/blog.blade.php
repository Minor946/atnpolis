<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 3/15/18
 * Time: 4:18 PM
 */
?>

@extends('layouts.layout')

@section('title', __('title.Blog'))

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    @include('components.blog.blog_body')
@endsection


@section('footer')
    @parent
@endsection
