@extends('layouts.layout')

@section('title', __('title.MainPage'))

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

    @section('content')
        @include('components.main.main_slider')
        @include('components.main.main_services')
        @include('components.main.main_counter')
        @include('components.main.main_action_block')
        @include('components.main.main_news')
    @endsection

<?php
        \App\Components\PagesComponents::updatedSearchAll();
?>

@section('footer')
    @parent
    <script>
        $(document).ready(function($) {
            var carousel = $('.carousel');
            carousel.carousel();

            $('#carousel').on('slid.bs.carousel', function () {
                $('.carousel-item').each(function( index ) {
                    if ($( this ).hasClass('active')) {
                        var video = $("#video"+index).get(0);
                        video.play();
                    }
                });
            });

            var flag_scroll=true;

            $(window).on('scroll' , function(){
                scroll_pos = $(window).scrollTop() + $(window).height();
                element_pos = $("#info-block").offset().top + $("#info-block").height() - 100 ;
                if (scroll_pos > element_pos && flag_scroll) {
                    flag_scroll = false;
                    $('.into-count').each(function () {
                        $(this).prop('Counter',0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 2000,
                            easing: 'swing',
                            step: function (now) {
                                var nf = new Intl.NumberFormat();
                                $(this).text(nf.format(Math.ceil(now)));
                            }
                        });
                    });
                }
            });
        });
        $('#write-us-phone').mask('(000) 00-00-00', {placeholder: "(000) 00-00-00", clearIfNotMatch: true});

        $('#form-write-us').on('submit', function(e){
            $.ajax({
                type		: "POST",
                url		    : "/callback",
                data		: $("#form-write-us").serializeArray(),
                cache		: false,
                success		: function(html) {
                    if(html['status']==="true"){
                        notie.alert({
                            type: 'success',
                            text: "{{__('modal.Success')}}",
                            stay: false,
                            time: 6,
                            position: 'bottom'
                        })
                    }
                }
            });
            e.preventDefault();
        });
    </script>
@endsection