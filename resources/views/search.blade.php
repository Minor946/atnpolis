<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/15/17
 * Time: 3:13 PM
 */
?>

@extends('layouts.layout')

@section('title', __('title.Search'))

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    @include('components.search.search_body')
@endsection


@section('footer')
    @parent
@endsection
