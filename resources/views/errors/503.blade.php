<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/12/18
 * Time: 3:51 PM
 */
?>

@extends('layouts.layout')

@section('title', '503 access denied')

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    <div class="wrapper row2">
        <div id="container bg-white" class="clear">
            <section id="fof" class="clear">
                <div class="hgroup">
                    <h1><span><strong>5</strong></span><span><strong>0</strong></span><span><strong>3</strong></span></h1>
                    <h2>Error ! <span>Access Denied</span></h2>
                </div>
                <p>You do not have the necessary permissions to post this page</p>
                <p><a href="javascript:history.go(-1)">&laquo; Go Back</a> / <a href="{{url('/')}}">Go Home &raquo;</a></p>
            </section>
        </div>
    </div>
@endsection

@section('footer')
    @parent
@endsection
