<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/12/18
 * Time: 3:51 PM
 */
?>

@extends('layouts.layout')

@section('title', 'FAQ')

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    <div class="wrapper row2">
        <div id="container bg-white" class="clear">
            <section id="fof" class="clear">
                <div class="hgroup">
                    <h1><span><strong class="blue-text">4</strong></span><span><strong class="yellow-text">0</strong></span><span><strong class="blue-text">4</strong></span></h1>
                    <h2>{{__("error.Error !")}} <span>{{__("error.Page Not Found")}}</span></h2>
                </div>
                <p>{{__("error.For Some Reason The Page You Requested Could Not Be Found On Our Server")}}</p>
                <p><a href="javascript:history.go(-1)">&laquo; {{__("error.Go Back")}}</a> /<a href="{{url('/')}}">{{__("error.Go Home")}} &raquo;</a></p>
            </section>
        </div>
    </div>
@endsection

@section('footer')
    @parent
@endsection
