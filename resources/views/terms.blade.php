<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/15/17
 * Time: 3:40 PM
 */

use App\Models\Pages;
use Illuminate\Support\Facades\Route;

$route = Route::getFacadeRoot()->current()->uri();
$page = Pages::findByAlias($type);
?>

@extends('layouts.layout')

@section('title', \App\Components\TranslateComponents::getName($page->id, \App\Components\TranslateComponents::TYPE_PAGES, LaravelLocalization::getCurrentLocale() ) )

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    @include('components.pages.terms_body')
@endsection


@section('footer')
    @parent
@endsection

