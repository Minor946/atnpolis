<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/15/17
 * Time: 3:18 PM
 */
?>

@extends('layouts.layout')

@section('title', __('title.AboutCompany'))

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    @include('components.about.about_body')
@endsection


@section('footer')
    @parent
@endsection

