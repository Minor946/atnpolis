<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/12/18
 * Time: 12:44 PM
 */
?>

<div class="container margin-bottom-30 bg-white shadow-v1">

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('messages.Contacts') }}</li>
        </ol>
    </nav>
    <div align="center">
        <h2>{{ __('messages.Contacts') }}</h2>
    </div>
    <img src="/images/lines.png" class="lines">

    <div class="post-body">
    <?php $towns = \App\Models\Subsidiary::all()->groupBy('region'); ?>
        <?php foreach ($towns as $town):?>
            <div class="title-subsidiary-group">
                <div class="title-line-grey"></div>
                <div class="title-subsidiary" align="center">
                    <h4>
                        <div>{{\App\Components\TranslateComponents::getTownName($town[0]->region,LaravelLocalization::getCurrentLocale())}}</div>
                    </h4>
                </div>
                <div class="subsidiary-list">
                    <?php $subsidiaries = \App\Models\Subsidiary::where(['region'=>$town[0]->region])->orderBy('type')->get();?>
                    <?php if(!empty($subsidiaries)):?>
                    <?php foreach ($subsidiaries as $subsidiary):?>
                        @include('items.subsidiary.item',[ 'subsidiary' => $subsidiary])
                    <?php endforeach; ?>
                    <?php endif;?>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    <img src="/images/brand_block.png" class="brand">
</div>