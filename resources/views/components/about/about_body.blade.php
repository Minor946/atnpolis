<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/16/18
 * Time: 1:34 PM
 */
?>

<div class="container margin-bottom-30 bg-white shadow-v1">
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('messages.AboutCompany') }}</li>
        </ol>
    </nav>
    <div align="center">
        <h2>{{ __('messages.AboutCompanyATNPolicy') }}</h2>
    </div>
    <img src="/images/lines.png" class="lines">

    <div class="post-body">
        <div align="center">
            <img class="big-logo" src="/images/big-logo.png">
        </div>
        <p class="margin-top-15">{!! __('about.AboutCompany') !!}</p>

        <div class="padding-bottom-15" align="left">
            <h4>{{ __('about.ATNNow') }}</h4>
            <p>{{ __('about.ATNNowDesc') }}</p>

        </div>

        <div>
            <div id="license"></div>
            <div class="row">
                <div class="col-md-12" align="center">
                    <h2>{{ __('messages.License') }}</h2>
                    <hr class="width-30">
                </div>
                <div class="certificate-block" align="center">
                    <a href="/images/cerificates/Compulsory_types_of_insurance.pdf" target="_blank">
                        <img src="/images/cerificates/Compulsory_types_of_insurance.png">
                    </a>
                    <a href="/images/cerificates/Reinsurance_license.pdf" target="_blank">
                        <img src="/images/cerificates/Reinsurance_license.png">
                    </a>
                    <a href="/images/cerificates/Voluntary_liability_insurance.pdf" target="_blank">
                        <img src="/images/cerificates/Voluntary_liability_insurance.png">
                    </a>
                    <a href="/images/cerificates/Voluntary_personal_insurance.pdf" target="_blank">
                        <img src="/images/cerificates/Voluntary_personal_insurance.png">
                    </a>
                    <a href="/images/cerificates/Voluntary_property_insurance.pdf" target="_blank">
                        <img src="/images/cerificates/Voluntary_property_insurance.png">
                    </a>

                </div>
            </div>
        </div>


    </div>
    <img src="/images/brand_block.png" class="brand">
</div>