<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/22/17
 * Time: 1:12 PM
 */

use App\Models\Post;
use Carbon\Carbon;

$post = Post::whereAlias($alias);

$dt = Carbon::parse($post->created_at);

?>

<div class="container margin-bottom-30 bg-white shadow-v1">
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }}</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/#news') }}">{{ __('messages.News') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$post->title}}</li>
        </ol>
    </nav>
    <div align="center">
        <h2>{{\App\Components\TranslateComponents::getName($post->id, \App\Components\TranslateComponents::TYPE_POST, LaravelLocalization::getCurrentLocale() )}}</h2>
        <div align="right">
            <small class="text-muted">{{ $dt->toDateString() }}</small>
        </div>
    </div>
    <img src="/images/lines.png" class="lines">
    <div class="post-body">
        <img class="post-body-img" src="{{ asset('/uploads/'.$post->post_logo) }}">
        <div class="post-text">
            {!! \App\Components\TranslateComponents::getDesc($post->id, \App\Components\TranslateComponents::TYPE_POST, LaravelLocalization::getCurrentLocale() ) !!}
        </div>
        <div class="post-gallery">
            <div id="lightgallery">
                    <?php if(!empty($post->post_gallery)):?>
                        <?php foreach ($post->post_gallery as $asset):?>
                        <a href="{{ asset('/uploads/'.$asset) }}">
                            <img src="{{ asset('/uploads/'.$asset) }}" />
                        </a>
                        <?php endforeach;?>
                    <?php endif;?>
            </div>
        </div>
    </div>
    <img src="/images/brand_block.png" class="brand">
</div>