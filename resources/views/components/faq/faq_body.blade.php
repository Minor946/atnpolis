<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/15/18
 * Time: 3:06 PM
 */
?>

<div class="container margin-bottom-30 bg-white shadow-v1">
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page"> {{ __('messages.Faq') }} </li>
        </ol>
    </nav>

    <section class="cd-faq">
        <?php $faqPage = \App\Models\Pages::where('type', \App\Models\Pages::TYPE_FAQ)->orderBy('position', 'asc')->get();?>
        <?php if(!empty($faqPage)):?>
            <ul class="cd-faq-categories">
                <?php foreach ($faqPage as $page):?>
                    <li><a href="#<?=$page->alias?>">
                            {{\App\Components\TranslateComponents::getName($page->id, \App\Components\TranslateComponents::TYPE_PAGES, LaravelLocalization::getCurrentLocale() )}}
                        </a></li>
                <?php endforeach;?>
            </ul>

            <div class="cd-faq-items">
                <?php foreach ($faqPage as $page):?>
                <div class="cd-faq-group">
                    <div id="<?=$page->alias?>"></div>
                    <h2  class="cd-faq-title">{{\App\Components\TranslateComponents::getName($page->id, \App\Components\TranslateComponents::TYPE_PAGES, LaravelLocalization::getCurrentLocale() )}}</h2>
                    {!! \App\Components\TranslateComponents::getDesc($page->id, \App\Components\TranslateComponents::TYPE_PAGES, LaravelLocalization::getCurrentLocale() ) !!}
                </div>
                <?php endforeach;?>
            </div>
        <?php endif;?>


    </section>
</div>