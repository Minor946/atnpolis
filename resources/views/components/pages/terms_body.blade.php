<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/12/18
 * Time: 4:38 PM
 */

use Illuminate\Support\Facades\Route;

$route = Route::getFacadeRoot()->current()->uri();
?>

<div class="container margin-bottom-30 bg-white shadow-v1">

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{\App\Components\TranslateComponents::getName($page->id, \App\Components\TranslateComponents::TYPE_PAGES, LaravelLocalization::getCurrentLocale() )}}</li>
        </ol>
    </nav>

    <div align="center">
        <h2>{{\App\Components\TranslateComponents::getName($page->id, \App\Components\TranslateComponents::TYPE_PAGES, LaravelLocalization::getCurrentLocale() )}}</h2>
    </div>
    <img src="/images/lines.png" class="lines">

    <div class="post-body">
        {!! \App\Components\TranslateComponents::getDesc($page->id, \App\Components\TranslateComponents::TYPE_PAGES, LaravelLocalization::getCurrentLocale() ) !!}
    </div>

    <img src="/images/brand_block.png" class="brand">
</div>