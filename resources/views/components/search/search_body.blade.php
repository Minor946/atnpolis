<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 3/7/18
 * Time: 1:00 PM
 */
?>

<div class="container margin-bottom-30 bg-white shadow-v1 height-500">
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{__('message.Search')}}: <strong>{{$query}}</strong></li>
        </ol>
    </nav>
    <div align="center">
        <h2>{{__("messages.SearchBySite")}}: "<strong><i>{{$query}}</i></strong>"</h2>
    </div>
    <img src="/images/lines.png" class="lines">
    <div class="post-body">
        @if (count($queries) === 0)
            <div class="wrapper row2">
                <div id="container bg-white" class="clear">
                    <section id="fof" class="clear" style="margin: 0">
                        <div class="hgroup">
                           <img src="/images/no_find.png">
                        </div>
                        <div align="center" style="margin-top: 15px">
                            <h3>{{__("search.Ooops, items not found")}}</h3>
                            <h5 class="text-muted">{{__("search.Try rewording your search or entering a new keyword")}}</h5>
                        </div>
                    </section>
                </div>
            </div>
        @else
            @foreach($queries as $item)

                @include('items.search.item',['type'=> $item->type, 'item'=>$item])
            @endforeach
        @endif
    </div>
    <img src="/images/brand_block.png" class="brand">
</div>
