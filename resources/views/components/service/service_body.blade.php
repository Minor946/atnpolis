<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/11/18
 * Time: 1:50 PM
 */

use App\Models\Services;
use Carbon\Carbon;

$dt = Carbon::now();

$service = Services::whereAlias($alias);
if(empty($service)){
    if($alias == "private_clients"){
        $has_child =  App\Models\Services::where('status', \App\Models\Services::SERVICE_SHOW)->where('parent_id', '=', null)
            ->where('type', \App\Models\Services::TYPE_PRIVATE)->orderBy('position')->get();
    }else if($alias == "corporate_clients"){
        $has_child =  App\Models\Services::where('status', \App\Models\Services::SERVICE_SHOW)->where('parent_id', '=', null)
            ->where('type', \App\Models\Services::TYPE_CORPORATE)->orderBy('position')->get();
    }
}else{
    $has_child =  App\Models\Services::where('parent_id', $service->id)->get();
}
?>

<div class="container margin-bottom-30 bg-white shadow-v1">
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }}</a></li>
            <li class="breadcrumb-item active">
                <?php if(($service != null && $service->type == Services::TYPE_PRIVATE) || $alias == "private_clients" ):?>
                    {{ __('messages.PrivateClients') }}
                <?php else: ?>
                    {{ __('messages.CorporateClients') }}
                <?php endif?>
            </li>
            <?php if($alias != "private_clients" && $alias != "corporate_clients"):?>
                <li class="breadcrumb-item active" aria-current="page">
                    {{\App\Components\TranslateComponents::getName($service->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                </li>
            <?php endif;?>
        </ol>
    </nav>
    <div align="center">
        <h2>{{\App\Components\TranslateComponents::getName($service->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}</h2>
    </div>
    <img src="/images/lines.png" class="lines">
    <?php if(count($has_child) > 0):?>
        <?php if(!empty($has_child)):?>
        <ul class="cards height-500">
    <?php foreach ($has_child as $child):?>
            <li class="cards__item">
                <a href="{{ url('service/'.\App\Components\ServiceComponents::getAlias($child->id) ) }}">
                    <div class="card-v2">
                        <div class="card__image" style="background-image: url(<?=\App\Components\ServiceComponents::getLogoName($child->id)?>);"></div>
                        <div class="card__content card__content_v2">
                            <div class="card__title card__title_v2">
                                {{\App\Components\TranslateComponents::getName($child->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endif;?>
    <?php else:?>
    <div class="post-body">
        <div class="post-text">
            {!! \App\Components\TranslateComponents::getDesc($service->id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() ) !!}
        </div>

        <div class="post-gallery">

            <div id="lightgallery">
                <?php if(!empty($service->gallery)):?>
                <?php foreach ($service->gallery as $asset):?>
                <a href="{{ asset('/uploads/'.$asset) }}">
                    <img src="{{ asset('/uploads/'.$asset) }}" />
                </a>
                <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>

    </div>
    <?php endif;?>

    <img src="/images/brand_block.png" class="brand">
</div>
