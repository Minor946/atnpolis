<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/15/18
 * Time: 12:53 PM
 */
?>
<div class="container margin-bottom-30 bg-white shadow-v1">
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page"> {{ __('messages.Employees') }} </li>
        </ol>
    </nav>


    <div align="center">
        <h2>{{ __('messages.Employees') }}</h2>
    </div>
    <img src="/images/lines.png" class="lines">

    <div class="post-body employees-list">
        <ul class="cards">
            <?php $workers = App\Models\Workers::orderBy('position')->get(); ?>
            <?php if(!empty($workers)):?>
            <?php foreach ($workers as $worker):?>
                @include('items.employees.item',['post'=>$worker])
            <?php endforeach;?>
            <?php endif;?>
        </ul>
    </div>
    <img src="/images/brand_block.png" class="brand">
</div>
