<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 3/15/18
 * Time: 4:18 PM
 */
?>

<div class="container container-post margin-bottom-30">
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }}</a></li>
        <?php if($type == \App\Models\Post::TYPE_BLOG):?>
            <li class="breadcrumb-item active">{{ __('messages.Blog') }}</li>
        <?php elseif ($type == \App\Models\Post::TYPE_NEWS):?>
            <li class="breadcrumb-item active">{{ __('messages.News') }}</li>
        <?php endif; ?>
        </ol>
    </nav>
    <div align="center">
        <h2>{{ __('messages.Blog') }}</h2>
    </div>
    <img src="/images/lines.png" class="lines">
    <div class="post-body">
        <?php $posts = App\Models\Post::where('type', $type)->where('status', \App\Models\Post::STATUS_SHOW)->orderBy('created_at','desc')->get(); ?>

        <?php if(!empty($posts)):?>
        <div class="container">
            <div class="row">
                <?php foreach ($posts as $key => $post):?>
                <?php if($key == 0):?>
                <div class="col-12  col-md-6">
                    @include('items.post.item_big_new',['post'=>$post])
                </div>
                <div class="col-12 col-md-6">
                    <div class="row">
                <?php elseif($key <= 4):?>
                    <div class="col-12  col-md-6">
                        @include('items.post.item_small_new',['post'=>$post])
                    </div>
                <?php if($key == 4 || (count($posts) < 5 && $key == count($posts) - 1 ) ):?>
                    </div>
                </div>
                <?php endif;?>
                <?php else:?>
                    <div class="col-12  col-md-3">
                        @include('items.post.item_small_new',['post'=>$post])
                    </div>
                <?php endif;?>
                <?php endforeach;?>
            </div>
        </div>
            <?php if(count($posts) == 1):?>
                </div>
            <?php endif;?>
        <?php endif;?>
    </div>
    <img src="/images/brand_block.png" class="brand">
</div>
<div class="clearfix"></div>
