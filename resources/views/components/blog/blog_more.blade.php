<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 3/15/18
 * Time: 4:50 PM
 */
?>

<?php $posts = App\Models\Post::orderBy('created_at')->take(5)->get(); ?>

<?php if(!empty($posts)):?>
    <div class="row">
        <?php foreach ($posts as $key => $post):?>
        <div class="col-12 col-md-3">
             @include('items.post.item_small_new',['post'=>$post])
        </div>
        <?php endforeach;?>
    </div>
<?php endif;?>