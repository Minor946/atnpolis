<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/13/17
 * Time: 2:35 PM
 */
?>

<div class="main-info-block shadow">
    <div class="container" align="center">
        <h2>{{ __('index.OurWorkIsInFigures') }}</h2>
        <hr class="width-30">
        <p>{{ __('index.ShortTextForAdvantages') }}</p>
        <div class="info-items" id="info-block" align="center">
            <div class="info-item">
                <p>{{ __('index.in2017WeHaveIssued') }}</p>
                <p> <h2 class="into-count">{{config('number_issued_policies')}}</h2></p>
                <p>{{ __('index.InsurancePolicies') }}</p>
            </div>
            <div class="info-item">
                <p>{{ __('index.for') }}</p>
                <p> <h2 class="into-count">{{config('number_clients')}}</h2></p>
                <p>{{ __('index.clients') }}</p>
            </div>
            <div class="info-item">
                <p>{{ __('index.andGiven') }}</p>
                <p> <h2 class="into-count display-inline">{{config('number_insurance')}}</h2><h2 class="display-inline"> {{ __('index.currency') }}</h2></p>
                <p>{{ __('index.insurancePayments') }}</p>
            </div>
        </div>
    </div>
</div>