<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/13/17
 * Time: 12:04 PM
 */
?>

<div class="swiper-container">
    <!-- Additional required wrapper -->

    <div id="carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php $countSlider = App\Models\Sliders::where('status', \App\Models\Sliders::STATUS_SHOW)->count();?>
            <?php  for ($i = 0; $i < $countSlider; $i++):?>
                <?php if($i == 0):?>
                    <li data-target="#carousel" data-slide-to="<?=$i?>" class="active"></li>
                <?php else: ?>
                <li data-target="#carousel" data-slide-to="<?=$i?>"></li>
                <?php endif;?>
            <?php endfor;?>
        </ol>
        <div class="carousel-inner">
        <?php $sliders = \App\Models\Sliders::where('status', \App\Models\Sliders::STATUS_SHOW)->get();?>
            <?php if(!empty($sliders)):?>
            <?php foreach($sliders as $key => $slide):?>
            <?php
            $url = 'javascript:void(0);';
            if(!empty($slide->link)){
                $url = $slide->link;
            }
            ?>
                <?php if($key == 0):?>
                    <div class="carousel-item video active"  data-interval="600">
                        <div class="overlay"></div>
                        <div class="carousel-caption d-md-block">
                            <h2><?=$slide->name?></h2>
                            <p><?=$slide->description?></p>
                        </div>
                        <a href="<?=$url?>">
                            <?php if($slide->type == \App\Models\Sliders::TYPE_VIDEO):?>
                                <video id="video<?=$key?>" src="{{ asset('/uploads/'.$slide->value) }}" poster="{{ asset('/uploads/'.$slide->poster) }}" autobuffer playsinline autoplay loop muted>
                                    <source src="{{ asset('/uploads/'.$slide->value) }}"></source>
                                </video>
                            <?php else:?>
                                <img class="carousel-image" src="{{ asset('/uploads/'.$slide->value) }}" alt="<?=$slide->name?>">
                            <?php endif;?>
                        </a>
                    </div>
                <?php else:?>
                    <div class="carousel-item video" data-interval="600">
                        <div class="overlay"></div>
                        <div class="carousel-caption d-md-block">
                            <h2><?=$slide->name?></h2>
                            <p><?=$slide->description?></p>
                        </div>
                        <a href="<?=$url?>">
                            <?php if($slide->type == \App\Models\Sliders::TYPE_VIDEO):?>
                                <video id="video<?=$key?>" src="{{ asset('/uploads/'.$slide->value) }}" poster="{{ asset('/uploads/'.$slide->poster) }}" preload="auto" autobuffer  playsinline autoplay loop muted>
                                    <source src="{{ asset('/uploads/'.$slide->value) }}"></source>
                                </video>
                            <?php else:?>
                                <img class="carousel-image" src="{{ asset('/uploads/'.$slide->value) }}" alt="<?=$slide->name?>">
                            <?php endif;?>
                        </a>
                    </div>
                <?php endif;?>
            <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>


</div>
