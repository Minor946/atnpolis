<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/13/17
 * Time: 5:15 PM
 */
?>

<div class="container container-post margin-bottom-30">
    <div class="row">
           <div class="col-md-12" align="center">
               <h2>{{ __('messages.News') }}</h2>
               <hr class="width-30">
           </div>
    </div>
    <div id="news"></div>
<?php $posts = App\Models\Post::where('status', \App\Models\Post::STATUS_SHOW)->orderBy('created_at', 'desc')->take(9)->get(); ?>

    <?php if(!empty($posts)):?>
    <div class="container">
        <div class="row">
            <?php foreach ($posts as $key => $post):?>
            <?php if($key == 0):?>
            <div class="col-12  col-md-6">
                @include('items.post.item_big_new',['post'=>$post])
            </div>
            <div class="col-12 col-md-6">
                <div class="row">
                    <?php elseif($key <= 4):?>
                    <div class="col-12  col-md-6">
                        @include('items.post.item_small_new',['post'=>$post])
                    </div>
                    <?php if($key == 4 || (count($posts) < 5 && $key == count($posts) - 1 )):?>
                </div>
            </div>
            <?php endif;?>
            <?php else:?>
            <div class="col-12  col-md-3">
                @include('items.post.item_small_new',['post'=>$post])
            </div>
            <?php endif;?>
            <?php endforeach;?>
        </div>
    </div>
    <?php endif;?>
</div>