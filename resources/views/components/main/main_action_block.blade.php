<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/14/17
 * Time: 1:48 PM
 */

use App\Models\Pages;
?>

<div class="main-action-block">
    <div class="container">
           <div class="row">
               <div class="col-md-3">
                   <a class="button button-full-wight btn-red" href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank">
                       <div class="button-commissar"></div>
                       <span>{{ __('action.CallOfTheEmergencyCommissioner') }}</span>
                   </a>
               </div>
               <div class="col-md-3">
                   <a class="button button-full-wight btn-green" href="{{ url('calculator') }}">
                       <div class="button-calculate"></div>
                       <span>{{ __('action.Insurance') }}<br>{{ __('action.Calculator') }}</span>
                   </a>
               </div>
               <div class="col-md-3">
                   <a class="button button-full-wight btn-blue" href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank">
                       <div class="button-phone"></div>
                       <span>{{ __('action.Call') }}<br>{{ __('action.ASpecialist') }}</span>
                   </a>
               </div>
               <div class="col-md-3">
                   <a class="button button-full-wight btn-yellow" href="{{ url('pages')}}/<?=Pages::find(2)->alias?>">
                       <div class="button-cloud"></div>
                       <span>{{ __('action.Insurance') }}<br>{{ __('action.Case') }}</span>
                   </a>
               </div>
           </div>
    </div>
</div>