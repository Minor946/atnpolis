<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/14/17
 * Time: 1:48 PM
 */
?>

<div class="main-service-block margin-height-30">
    <div class="container" align="center">
        <h2>{{ __('messages.Services') }}</h2>
        <hr class="width-30">
        <ul class="cards">
            <?php $serviceMain = \App\Models\ServicesMain::orderBy('position', 'asc')->get(); ?>
            <?php if(!empty($serviceMain)):?>
                <?php foreach ($serviceMain as $item):?>
                <li class="cards__item">
                    <a href="{{ url('service/'.\App\Components\ServiceComponents::getAlias($item->service_id) ) }}">
                        <div class="card-v2">
                            <div class="card__image" style="background-image: url(../uploads/<?=\App\Components\ServiceComponents::getLogoName($item->service_id)?>);"></div>
                            <div class="card__content">
                                <div class="card__title">
                                    {{\App\Components\TranslateComponents::getName($item->service_id, \App\Components\TranslateComponents::TYPE_SERVICE, LaravelLocalization::getCurrentLocale() )}}
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <?php endforeach; ?>
            <?php endif;?>
        </ul>
    </div>
</div>
