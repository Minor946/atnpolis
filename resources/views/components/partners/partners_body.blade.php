<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/11/18
 * Time: 5:07 PM
 */
?>

<div class="container margin-bottom-30 bg-white shadow-v1">

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }} </a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('messages.Partners') }}</li>
        </ol>
    </nav>

    <div align="center">
        <h2>{{ __('messages.Partners')}}</h2>
    </div>
    <img src="/images/lines.png" class="lines">


    <div class="post-body">
        <div class="row">
            <div class="col-md-12">

                <?php $groups = App\Models\PartnersGroups::all()->sortBy("position"); ?>
                <?php if(!empty($groups)):?>
                <?php foreach ($groups as $group):?>
                <?php $partners_list = \App\Models\PartnersHasGroups::where('partners_group_id', $group->id)->get();?>
                <?php if(count($partners_list) > 0):?>
                <h4 class="text-muted">{{\App\Components\TranslateComponents::getName($group->id, \App\Components\TranslateComponents::TYPE_PARTNERS_GROUP_NAME, LaravelLocalization::getCurrentLocale() )}}</h4>
                    <a id="partners<?=$group->id?>"></a>
                <hr class="hr-light">
                <?php if(!empty($partners_list)):?>
                <div class="row partners-list">
                    <?php foreach ($partners_list as $item):?>
                    <div class="col-sm-12 col-md-6">
                        @include('items.partners.item',['partner_id'=> $item->partners_id])
                    </div>
                    <?php endforeach;?>
                </div>
                <?php endif;?>
                <?php endif;?>
                <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>
    </div>


    <img src="/images/brand_block.png" class="brand">
</div>