<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 1/4/18
 * Time: 4:48 PM
 */

use App\Models\Pages;
?>
<div class="container margin-bottom-30 padding-bottom-15 bg-white shadow-v1">
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('messages.Home') }} </a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('messages.Calculator') }}</li>
        </ol>
    </nav>
    <img src="/images/lines.png" class="lines">
    <div class="row">
        <div class="col-md-7">
            <label for="calc-type-select">
                <small>{{ __('messages.Calculator') }}</small>
            </label>
            <br>
            <select class="js-example-basic-single js-states form-control" style="width: 60%" id="calc-type-select">
                   <?php $calcs_type = App\Models\Calculator::where('status', \App\Models\Calculator::CALC_SHOW)->orderBy('position')->take(10)->get(); ?>
                   <?php if(!empty($calcs_type)):?>
                   <?php foreach ($calcs_type as $calc_type):?>
                    <?php if(!empty($type) && $type == $calc_type->alias):?>
                       <option selected value="<?=$calc_type->alias?>">{{\App\Components\TranslateComponents::getName($calc_type->id, \App\Components\TranslateComponents::TYPE_CALC, LaravelLocalization::getCurrentLocale() )}}</option>
                    <?php else:?>
                       <option value="<?=$calc_type->alias?>">{{\App\Components\TranslateComponents::getName($calc_type->id, \App\Components\TranslateComponents::TYPE_CALC, LaravelLocalization::getCurrentLocale() )}}</option>
                    <?php endif;?>
                   <?php endforeach;?>
                   <?php endif;?>
            </select>
            <hr class="hr-light">

            <div id="calc-form" class="calc-form">
                <p><strong>{{App\Components\CalculatorComponents::getSelectedName($type,LaravelLocalization::getCurrentLocale())}}:</strong></p>
                @include('components.calculator.calculator_form',['type' => $type])
            </div>
                <hr class="hr-light">
                    <div class="block-premium row">
                        <div class="col-md-3 label-premium">
                            <small>{{ __('messages.Award') }} <br> {{ __('messages.ToPay') }}  </small>
                        </div>
                        <div class="col-md-9"><h2 id="premium-price">0.00</h2></div>
                    </div>
                <hr class="hr-light">
                {{ Form::open(array ('url'=>'/add-offer', 'method' => 'post', 'id'=>'form-offer')) }}
                <div class="form-group">
                    {{ Form::label(__('messages.Name'), null, ['class' => 'control-label']) }}
                        {{ Form::text("name", "", array_merge(['class' => 'form-control', 'id'=>'form-offer-name', 'placeholder'=>__('messages.Name')])) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label(__('messages.Phone'), null, ['class' => 'control-label required']) }}
                        {{ Form::text("phone", "", array_merge(['class' => 'form-control', 'required'=>true, 'id'=>'form-offer-phone', 'placeholder'=>__('messages.Phone')])) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label(__('messages.Message'), null, ['class' => 'control-label']) }}
                        {{ Form::textarea("message", "", array_merge(['class' => 'form-control', 'required'=>false, 'id'=>'form-offer-msg', 'placeholder'=>__('messages.Message'), 'rows'=>3])) }}
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-3">
                            <img src="{{ captcha_src() }}" alt="captcha" class="captcha-img" data-refresh-config="default">
                        </div>
                        <div class="col-sm-3">
                            {{ Form::text("captcha", "", array_merge(['class' => 'form-control', 'required'=>true, 'id'=>'form-offer-captcha', 'placeholder'=>__('messages.code')])) }}
                            <span class="text-error hide" id="capcha-error">{{__('messages.InputCorrectCode')}}</span>
                        </div>
                    </div>

                    <div align="center">
                        {{Form::submit(__('messages.Checkout'), ['class' => 'btn btn-success btn-submit'])}}
                    </div>
                    {{ Form::close() }}
        </div>
        <div class="col-md-4">
            <div class="main-action-block">
                <div class="container">
                    <div class="row">
                        {{--<div class="col-md-12">--}}
                            {{--<a class="button button-full-wight btn-red" href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank">--}}
                                {{--<div class="button-commissar"></div>--}}
                                {{--<span>{{ __('action.CallOfTheEmergencyCommissioner') }}</span>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-12">--}}
                            {{--<a class="button button-full-wight btn-blue" href="https://api.whatsapp.com/send?phone={{config('phone_whatsapp_draft')}}" target="_blank">--}}
                                {{--<div class="button-phone"></div>--}}
                                {{--<span>{{ __('action.Call') }}<br>{{ __('action.ASpecialist') }}</span>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        <div class="col-md-12">
                            <a class="button button-full-wight btn-yellow"  href="{{ url('pages')}}/<?=Pages::find(2)->alias?>">
                                <div class="button-cloud"></div>
                                <span>{{ __('action.Insurance') }}<br>{{ __('action.Case') }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#form-offer-phone').mask('(000) 00-00-00', {placeholder: "(000) 00-00-00", clearIfNotMatch: true});

        $('#form-offer').on('submit', function(e){
            $.ajax({
                type		: "POST",
                url		    : "/add-offer",
                data		: $("#form-offer, #form-calc-result").serialize(),
                cache		: false,
                success		: function(html) {
                    if(html['status']==="true"){
                        $("#capcha-error").hide();
                        notie.alert({
                            type: 'success',
                            text: "{{__('modal.SuccessCalculator')}}",
                            stay: false,
                            time: 6,
                            position: 'bottom'
                        })
                    }else{
                        $(".captcha-img").click();
                        $("#capcha-error").show();
                        notie.alert({
                            type: 'error',
                            text: "{{__('messages.InputCorrectCode')}}",
                            stay: false,
                            time: 6,
                            position: 'bottom'
                        })
                    }
                }
            });
            e.preventDefault();
        });

    });
</script>