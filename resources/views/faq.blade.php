<?php
/**
 * Created by PhpStorm.
 * User: minor946
 * Date: 12/15/17
 * Time: 3:41 PM
 */
?>

@extends('layouts.layout')

@section('title', __('title.FAQ'))

@section('sidebar')
    @parent
@endsection

@section('menu')
    @parent
@endsection

@section('content')
    @include('components.faq.faq_body')
@endsection


@section('footer')
    @parent
@endsection
